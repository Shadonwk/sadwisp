<%@ page import="mx.com.wisnet.sadwisp.seguridad.Rol" %>
<div class="row">
    <hr>
    <div class="form-group col-md-4">
        <label class="control-label" for="username"><g:message code="usuario.username.label"/></label>
        <g:textField name="username" class="form-control" maxlength="50" required="" value="${usuario.username}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="password"><g:message code="usuario.password.label"/></label>
        <g:passwordField name="password" class="form-control" maxlength="50" required="" value="${usuario.password}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="nombre"><g:message code="usuario.nombre.label"/></label>
        <g:textField name="nombre" class="form-control" maxlength="50" required="" value="${usuario.nombre}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="apellidoPaterno"><g:message code="usuario.apellido.paterno.label"/></label>
        <g:textField name="apellidoPaterno" class="form-control" maxlength="50" required="" value="${usuario.apellidoPaterno}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="apellidoMaterno"><g:message code="usuario.apellido.materno.label"/></label>
        <g:textField name="apellidoMaterno" class="form-control" maxlength="50" required="" value="${usuario.apellidoMaterno}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="email"><g:message code="usuario.email.label"/></label>
        <g:textField name="email" class="form-control" maxlength="150" required="" value="${usuario.email}"/>
    </div>
    <div class="form-group col-md-4">
        <g:select class="form-control" name="roles"
                  from="${mx.com.wisnet.sadwisp.seguridad.Rol.list()}"
                  optionKey="id"
                  optionValue="nombre"
                  multiple="true"
                  required=""/>
    </div>

</div>

