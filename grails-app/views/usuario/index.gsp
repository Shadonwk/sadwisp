<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

    <g:link class="btn btn-primary btn-block col-sm-2 col-md-2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>
    <br>

    <div id="list-usuario" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>


        <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
            <thead>
            <tr>
                <th>Username</th>
                <th>Nombre</th>
                <th>Activo</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${usuarioList}" var="item">
                <tr>

                    <td>
                        <g:link action="show" id="${item.id}">
                            <g:fieldValue bean="${item}" field="username" />
                        </g:link>
                    </td>
                    <td><g:fieldValue bean="${item}" field="nombre" /></td>
                    <td><g:fieldValue bean="${item}" field="enabled" /></td>




                </tr>
            </g:each>
            </tbody>
        </table>



        <div class="pagination">
                <g:paginate total="${usuarioCount ?: 0}" />
            </div>
        </div>
    </body>
</html>