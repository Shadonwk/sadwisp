<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
                <div id="show-usuario" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if><br>

                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <tbody>
                        <tr>
                            <th class="span3"><g:message code="usuario.username.label" default="Usuario"/></th>
                            <td><g:fieldValue bean="${usuario}" field="username" /></td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="usuario.nombre.label" default="Usuario"/></th>
                            <td><g:fieldValue bean="${usuario}" field="nombreCompleto" /></td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="usuario.role.label" default="Usuario"/></th>
                            <td><g:fieldValue bean="${usuario}" field="role" /></td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="usuario.enabled.label" default="Usuario"/></th>
                            <td><g:fieldValue bean="${usuario}" field="enabled" /></td>
                        </tr>
                        </tbody>
                    </table>

            <g:form resource="${this.usuario}" method="DELETE">
                <fieldset class="buttons">
%{--                    <g:link class="btn btn-primary" style="pointer-events: none;" action="edit" resource="${this.usuario}"><g:message code="default.button.edit.label" default="Edit" /></g:link>--}%
                    <g:link class="btn btn-primary" action="edit" resource="${this.usuario}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
