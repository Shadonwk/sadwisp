<!doctype html>
<html>
<head>
    <meta name="layout" content="inicio"/>
    <title>SadWisp</title>




</head>

<body>

<h3>Reportes</h3>
<hr>
<br>

<g:form action="generarReporte">
    <div class="form-group row m-b-12">
        <div class="col-md-6 col-sm-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Fecha Inicio:</label>
                <div class="col-md-8 col-sm-8">
                    <div class="input-group date" id="datetimepickerfechainicio">
                        <g:datePicker name="fechaInicio" value="${fechaInicio}"
                                      precision="day" noSelection="['':'-Choose-']"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Fecha Fin:</label>
                <div class="col-md-8 col-sm-8">
                    <div class="input-group date" id="datetimepickerfechafin">
                        <g:datePicker name="fechaFin" value="${fechaFin}"
                                      precision="day" noSelection="['':'-Choose-']"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Usuario:</label>
                <div class="col-md-8 col-sm-8">
                    <div class="input-group date" id="tienda">
                        <g:select name="usuario" from="${usuarios}" data-live-search="true" data-style="btn-white"
                                  optionKey="id"  optionValue="nombreCompleto" class="form-control selectpicker"
                                  noSelection="['':'Selecciona un usuario']" value="${this.usuario?.id}"   id="selectUsuario"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <g:submitButton name="buscar" value="Buscar" class="btn btn-primary m-r-5 m-b-5"/>
        </div>
        <div class="col-md-6 align-items-end">
            <label class="col-md-4 col-md-pull-1 col-form-label">Total acumulado: $ ${sumatoria}</label>
        </div>
    </div>

    <br>
    <br>
    <table id="noscroll-data-table-reporteAjuste" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th class="text-nowrap">Cliente</th>
            <th class="text-nowrap">Fecha pago</th>
            <th class="text-nowrap">Importe</th>
            <th class="text-nowrap">Usuario Cobro</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${pagos}" var="item">
            <tr>

                <td><g:fieldValue bean="${item}" field="cliente.nombreCompleto" /></td>
                <td><g:fieldValue bean="${item}" field="fechaPago" /></td>
                <td><g:fieldValue bean="${item}" field="cantidad" /></td>
                <td><g:fieldValue bean="${item}" field="usuarioCobro" /></td>

            </tr>
        </g:each>
        </tbody>
    </table>
</g:form>

</body>
</html>
