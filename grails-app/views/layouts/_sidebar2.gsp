<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

    <sec:ifAllGranted roles="ROLE_ADMINISTRADOR">
        <li class="nav-item active">
            <g:link class="nav-link" controller="usuario" action="index">
                <i class="fas fa-fw fa-user-alt"></i>
                <span>Usuarios</span>
            </g:link>
        </li>


        <li class="nav-item active">
            <g:link class="nav-link" controller="escritorio" action="index">
                <i class="fas fa-fw fa-chart-bar"></i>
                <span>Dashboard</span>
            </g:link>
        </li>


        <li class="nav-item active">
            <g:link class="nav-link" controller="configuracion" action="edit" id="1">
                <i class="fas fa-fw fa-chart-bar"></i>
                <span>Configuración</span>
            </g:link>
        </li>

        <li class="nav-item active">
            <g:link class="nav-link" controller="mikrotik" action="index">
                <i class="fas fa-fw fa-laptop"></i>
                <span>Mikrotiks</span></g:link>
        </li>

        <li class="nav-item active">
            <g:link class="nav-link" controller="plan" action="index">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Planes</span></g:link>
        </li>


        <li class="nav-item active">
            <g:link class="nav-link" controller="reportes" action="index">
                <i class="fas fa-fw fa-list-alt"></i>
                <span>Reportes (beta)</span></g:link>
        </li>


        </sec:ifAllGranted>

    <sec:ifAnyGranted roles="ROLE_ADMINISTRADOR,ROLE_COBRADOR">
        <li class="nav-item active">
            <g:link class="nav-link" controller="cliente" action="index">
                <i class="fas fa-fw fa-address-card"></i>
                <span>Clientes</span>
            </g:link>
        </li>

        <li class="nav-item active">
            <g:link class="nav-link" controller="cliente" action="clientesBloqueados">
                <i class="fas fa-fw fa-user-circle"></i>
                <span>Suspendidos</span>
            </g:link>
        </li>
    </sec:ifAnyGranted>

    <sec:ifAnyGranted roles="ROLE_ADMINISTRADOR, ROLE_TECNICO">
        <li class="nav-item active">
        <g:link class="nav-link" controller="ticket" action="index">
            <i class="fas fa-fw fa-ticket-alt"></i>
            <span>Soporte</span>
        </g:link>

        </li>
    </sec:ifAnyGranted>

    </ul>

    <div id="content-wrapper">
        <div class="container-fluid">

            <g:layoutBody/>
        </div>

        <g:render template="/layouts/footer"/>
    </div>


</div>