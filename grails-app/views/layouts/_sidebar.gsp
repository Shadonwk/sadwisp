
    <ul class="nav sidebar">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Administración</span>
        </h6>
        <g:link controller="usuario" action="index">Usuarios</g:link>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Catalogos</span>
        </h6>
        <g:link controller="area" action="index">Areas</g:link>
        <g:link controller="tramite" action="index">Tramites</g:link>
        <g:link controller="tipoIdentificacion" action="index">Identificaciones</g:link>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Pre Registro</span>
        </h6>
        <g:link controller="defensor" action="index">Defensores</g:link>
        <g:link controller="proveedor" action="index">Proveedores</g:link>
        <g:link controller="visitaRecurrente" action="index">Recurrentes</g:link>

        %{--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--}%
            %{--<span>Acceso</span>--}%
        %{--</h6>--}%
        %{--<g:link controller="controlAcceso" action="index">Accesos</g:link>--}%
    </ul>

