<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><g:layoutTitle default="SADWISP"/></title>

    <g:layoutHead/>
    <!-- Custom styles for this template-->

    <asset:stylesheet src="sb-admin.css"/>
    <asset:stylesheet src="bo/bootstrap.css"/>
    <asset:stylesheet src="fontawesome-free/css/all.css"/>
    <asset:stylesheet src="datatables/dataTables.bootstrap4.css"/>

    <asset:javascript src="application.js"/>

    <asset:javascript src="application.css"/>
    <asset:stylesheet href="colores.css"/>


    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <style>
    .nopadding {
    padding: 0 !important;
    margin: 0 !important;
    }
</style>
</head>

<body id="page-top" style="background-color: #ffffff">

<g:render template="/layouts/header"/>
<g:render template="/layouts/sidebar2"/>

<div class="container nopadding">



    <g:render template="/layouts/footer"/>



</div>
</body>
</html>
