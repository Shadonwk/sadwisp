%{--<div class="navbar navbar-default navbar-static-top"  role="navigation">--}%
  %{--<asset:image src="sadLogo.png" />--}%
%{--</div>--}%

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">SadWisp</a>



    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </form>


    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
            <g:link controller="notificacion" action="index" class="nav-link dropdown-toggle" >
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-danger">${mx.com.wisnet.sadwisp.Notificacion.count()}</span>
            </g:link>
        </li>



        <li class="nav-item no-arrow">
            <g:link controller='logout'>
                <span >SALIR</span></g:link>
        </li>


</ul>

</nav>



