<footer class="sticky-footer">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright © http://wisnet.com.mx 2018 v.1.0.5 </span>
    </div>
  </div>
</footer>

  <!-- Bootstrap core JavaScript-->

  <asset:javascript src="jquery/jquery.min.js"/>
  <asset:javascript src="bo/js/bootstrap.bundle.min.js"/>

  <!-- Core plugin JavaScript-->

  <asset:javascript src="jquery-easing/jquery.easing.min.js"/>

  <!-- Page level plugin JavaScript-->

  <asset:javascript src="chart.js/Chart.min.js"/>
  <asset:javascript src="datatables/jquery.dataTables.js"/>
  <asset:javascript src="datatables/dataTables.bootstrap4.css"/>

