<div class="row">

    <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="asunto"><g:message code="ticket.asunto.label"/></label>
        <g:textField name="asunto" class="form-control" maxlength="100" required="" value="${ticket.asunto}"/>
    </div>
</div>
<div class="row">
    <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="descripcion"><g:message code="ticket.descripcion.label"/></label>
        <g:textArea name="descripcion" class="form-control" maxlength="250" required="" value="${ticket.descripcion}"/>
    </div>
</div>

<div class="row">
    <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="costo"><g:message code="ticket.costo.label"/></label>
        <g:field type="number" name="costo" class="form-control" maxlength="20" required="" value="${ticket.costo}"/>
    </div>
</div>

<div class="row">
    <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="cliente"><g:message code="ticket.cliente.label"/></label>
        <g:select name="cliente" class="mySelect form-control" id="cliente"
              from="${clienteList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              optionKey="id"
              required="true"

        />
    </div>
</div>

<div class="row">
    <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="prioridad"><g:message code="ticket.prioridad.label"/></label>
        <g:select name="prioridad" class="form-control"
                  from="['Baja','Media', 'Alta', 'Urgente']"
                  noSelection="${['':message(code:'plan.pcq.select.label')]}"
                  required=""/>
    </div>
</div>

<div class="row">
   <div class="form-group offset-3 col-md-4">
        <label class="control-label" for="tecnicos"><g:message code="ticket.tecnico.label"/></label>

        <g:select class="form-control" name="tecnicos"
                  from="${tecnicoList}"
                  optionKey="id"
                  optionValue="nombre"
                  multiple="true"
                  required=""/>
    </div>
</div>
<script>
    $('.mySelect').select2();
</script>