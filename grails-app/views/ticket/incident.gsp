<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="inicio" />
    <g:set var="entityName" value="${message(code: 'configuracion.label', default: 'Incidencia')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>

<div id="create-configuracion" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.incidencia}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.incidencia}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form controller="ticket" action="saveIncident" method="POST">
        <fieldset class="form">

            <div class="row">

                <div class="form-group offset-3 col-md-4">
                    <label class="control-label" for="ticket"><g:message code="ticket.asunto.label"/></label>
                    <g:textField name="ticketId" class="form-control" maxlength="100"  value="${incidencia.ticket.id}"/>
                    <g:textField name="ticket" class="form-control" maxlength="100" disabled="true" value="${incidencia.ticket.asunto}"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-3 col-md-4">
                    <label class="control-label" for="descripcion"><g:message code="ticket.descripcion.label"/></label>
                    <g:textArea name="descripcion" class="form-control" maxlength="250" disabled="true" required="" value="${incidencia.ticket.descripcion}"/>
                </div>
            </div>

            <div class="row">
                <div class="form-group offset-3 col-md-4">
                    <label class="control-label" for="descripcion"><g:message code="ticket.descripcion.incidencia.label"/></label>
                    <g:textArea name="descripcion" class="form-control" maxlength="250" required="" value="${incidencia.descripcion}"/>
                </div>
            </div>

        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="btn btn-outline-danger" value="${message(code: 'default.button.incidente.create.label', default: 'Create')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
