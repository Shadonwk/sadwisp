<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="inicio" />
    <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Ticket')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div id="show-usuario" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if><br>

    <table class="table table-striped table-bordered table-condensed table-hover">
        <tbody>
        <tr>
            <th class="span3"><g:message code="ticket.asunto.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="asunto" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.descripcion.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="descripcion" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.costo.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="costo" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.cliente.label" default="Usuario"/></th>
            <td>
                <g:link controller="cliente" action="show" id="${ticket.cliente.id}">
                    <g:fieldValue bean="${ticket}" field="cliente" />
                </g:link>
            </td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.cliente.telefono.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="cliente.telefono" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.prioridad.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="prioridad" /></td>
        </tr>

        <tr>
            <th class="span3"><g:message code="ticket.status.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="status" /></td>
        </tr>
        </tbody>
    </table>
    <br>

    <g:if test="${ticket.status.equals("CERRADO")}">

        <table class="table table-striped table-bordered table-condensed table-hover">
            <tbody>
            <tr>
                <th class="span3"><g:message code="ticket.costo.final.label" default="Usuario"/></th>
                <td><g:fieldValue bean="${ticket}" field="costoFinal" /></td>
            </tr>
            <tr>
                <th class="span3"><g:message code="ticket.descripcion.cierre.label" default="Usuario"/></th>
                <td><g:fieldValue bean="${ticket}" field="descripcionCierre" /></td>
            </tr>
            </tbody>
        </table>

    </g:if>
    <br>



    <g:if test="${ticket.incidencias}">
        <h3>Incidencias</h3>
        <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
            <thead>
            <tr>
                <th>Incidencia</th>
                <th>Fecha</th>
                <th>Tecnico</th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${ticket.incidencias}" var="item">
                <tr>
                    <td><g:fieldValue bean="${item}" field="descripcion" /></td>
                    <td><g:fieldValue bean="${item}" field="dateCreated" /></td>
                    <td><g:fieldValue bean="${item}" field="userCreated" /></td>

                </tr>
            </g:each>

            </tbody>
        </table>


    </g:if>

    <br>

    <g:form resource="${this.usuario}" method="DELETE">
        <fieldset class="buttons">
            %{--                    <g:link class="btn btn-primary" style="pointer-events: none;" action="edit" resource="${this.usuario}"><g:message code="default.button.edit.label" default="Edit" /></g:link>--}%
            <g:if test="${ticket.status.equals("ASIGNADO")}">
                <g:link class="btn btn-outline-success" action="procesar" resource="${this.ticket}"><g:message code="default.button.process.label" default="Procesar" /></g:link>
            </g:if>

            <g:if test="${ticket.status.equals("EN PROCESO")}">
                <g:link class="btn btn-outline-danger" action="incident" resource="${this.ticket}"><g:message code="default.button.incident.label" default="Incidencia" /></g:link>
                <g:link class="btn btn-outline-primary" action="cerrar" resource="${this.ticket}"><g:message code="default.button.close.label" default="Cerrar" /></g:link>
            </g:if>

            <sec:ifAnyGranted roles="ROLE_ADMINISTRADOR">
                <g:link class="btn btn-primary" action="edit" resource="${this.ticket}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </sec:ifAnyGranted>
        </fieldset>
    </g:form>
</div>
</body>
</html>
