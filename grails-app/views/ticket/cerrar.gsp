<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="inicio" />
    <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Ticket')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div id="show-usuario" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if><br>

    <table class="table table-striped table-bordered table-condensed table-hover">
        <tbody>
        <tr>
            <th class="span3"><g:message code="ticket.asunto.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="asunto" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.descripcion.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="descripcion" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.costo.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="costo" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.cliente.label" default="Usuario"/></th>
            <td>
                <g:link controller="cliente" action="show" id="${ticket.cliente.id}">
                    <g:fieldValue bean="${ticket}" field="cliente" />
                </g:link>
            </td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.cliente.telefono.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="cliente.telefono" /></td>
        </tr>
        <tr>
            <th class="span3"><g:message code="ticket.prioridad.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="prioridad" /></td>
        </tr>

        <tr>
            <th class="span3"><g:message code="ticket.status.label" default="Usuario"/></th>
            <td><g:fieldValue bean="${ticket}" field="status" /></td>
        </tr>
        </tbody>
    </table>

    <g:form resource="${ticket}" action="saveClose" method="POST">
        <fieldset class="form">
            <div class="row">

                <div class="form-group offset-3 col-md-4">
                    <label class="control-label" for="costoFinal"><g:message code="ticket.costo.final.label"/></label>
                    <g:textField name="costoFinal" class="form-control" maxlength="100" required="" value="${ticket.costoFinal}"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-3 col-md-4">
                    <label class="control-label" for="descripcionCierre"><g:message code="ticket.descripcion.cierre.label"/></label>
                    <g:textArea name="descripcionCierre" class="form-control" maxlength="250" required="" value="${ticket.descripcionCierre}"/>
                </div>
            </div>

        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="btn btn-outline-success" value="${message(code: 'default.button.close.label', default: 'Cerrar')}" />
        </fieldset>
    </g:form>


</div>
</body>
</html>
