<html>
<head>
    <meta name="layout" content="inicio" />
    <g:set var="entityName" value="${message(code: 'mikrotik.label', default: 'Soporte')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>

<div class="nav" role="navigation">
   <sec:ifAnyGranted roles="ROLE_ADMINISTRADOR">
        <g:link class="btn btn-primary btn-block col-md-2 col-sm-2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>
   </sec:ifAnyGranted>
</div>
<br>
<div id="list-ticket" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <br>

    <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
        <thead>
        <tr>
            <th>Num. Ticket</th>
            <th>Cliente</th>
            <th>Asunto</th>
            <th>Prioridad</th>
            <th>Tecnico</th>
            <th>Estatus</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${ticketList}" var="item">
            <tr>

                <td>

                    <g:link action="show" id="${item.id}">
                        ${item.id}
                    </g:link>
                </td>
                <td>
                    <g:link action="show" controller="cliente" id="${item.cliente.id}">
                        <g:fieldValue bean="${item}" field="cliente" />
                    </g:link>
                </td>
                <td><g:fieldValue bean="${item}" field="asunto" /></td>
                <td><g:fieldValue bean="${item}" field="prioridad" /></td>
                <td><g:fieldValue bean="${item}" field="tecnicos" /></td>
                <td><g:fieldValue bean="${item}" field="status" /></td>

            </tr>
        </g:each>

        </tbody>
    </table>


    <div class="pagination">
        <g:paginate total="${ticketCount ?: 0}" />
    </div>
</div>
</body>
</html>