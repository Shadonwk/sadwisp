<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'historial.label', default: 'Historial')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="show-historial" class="content scaffold-show" role="main">
            <h2>Historial pago ${cliente}</h2>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <br>

            <div class="container">
                <div class="table-responsive">
                    <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
                        <thead>
                        <tr>
                            <th>Mes</th>
                            <th>Status</th>
                            <g:if test="${editarNota}">
                                <th>Nombre Nota:</th>
                            </g:if>
                            <th>Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${historialList}" var="item">
                            <g:form action="printNote">
                                <tr>

                                    <td>${item.getMesLetra()} ${item.anio}<g:hiddenField name="id" value="${item.id}"/></td>
                                <td>
                                    <g:if test="${item.statusPago}">
                                        Pagado
                                    </g:if>
                                    <g:else>
                                        --
                                    </g:else>
                                </td>
                                <g:if test="${editarNota}">
                                    <td>
                                        <g:textField name="name" />
                                    </td>
                                </g:if>
                                <td>
                                    <g:if test="${item.statusPago}">
                                        <g:link  action="printNote" id="${item.id}"><i class="fas fa-print fa-2x iconoNota"></i></g:link>
                                    </g:if>
                                    <g:else>
                                        <g:link  action="sendPayment" id="${item.id}"><i class="fas fa-money-bill-alt fa-2x iconoPago"></i></g:link>
                                    </g:else>

%{--                                    <sec:ifAllGranted roles="ROLE_ADMINISTRADOR">--}%
%{--                                        <g:link class="btn btn-danger" action="suspenderServicio" resource="${item}"><g:message code="historial.button.nota.label" default="Suspender Servicio" /></g:link>--}%
%{--                                        <g:link class="btn btn-success" action="reanudarServicio" id="${item.id}"><g:message code="historial.button.pago.label" default="Reactivar Servicio" /></g:link>--}%
%{--                                    </sec:ifAllGranted>--}%

                                </td>
                            </g:form>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>

                </div>
            </div>


        </div>
    </body>
</html>
