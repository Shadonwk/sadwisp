<!doctype html><g:set var='sec' value='${applicationContext.springSecurityService.securityConfig}'/>
<g:set var="logo" value="${mx.com.wisnet.sadwisp.Configuracion.get(1).nombreLogo}"/>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <s2ui:title messageCode='spring.security.ui.login.title'/>
    <asset:stylesheet href="login"/>


<body class="login-bg">
<section class="login-box" style="text-align: center">


    <s2ui:form class="login-form" style="text-align: center" type='login' focus='username'>
        <asset:image src="/logos/${logo}" id="cafi" heigth="180" width="180"/>

        <asset:image src="sadwisp.svg" id="logo" heigth="10px"/>


        <input id="username" type="text" class="mt-50"
               name="${sec.apf.usernameParameter}"
               placeholder="${message(code: 'spring.security.ui.login.username')}">

        <input id="password" type="password" class="mt-30"
               name="${sec.apf.passwordParameter}"
               placeholder="${message(code: 'spring.security.ui.login.password')}">

        <div class="mt-20 ${flash.message ? '' : 'hidden'}">
            <p class="error">${flash.message ?: ''}</p>
        </div>

        <button id="submit" type='submit' class="btn btn-primary btn-lg mt-10">Entrar</button>

        <div class="mt-20">


        </div>
    </s2ui:form>

</section>
</body>
</html>
