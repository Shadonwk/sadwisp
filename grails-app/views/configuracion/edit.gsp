<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'configuracion.label', default: 'Configuracion')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>

    </head>
    <body>

        <div id="edit-configuracion" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.configuracion}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.configuracion}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.configuracion}" method="PUT">
                <g:hiddenField name="version" value="${this.configuracion?.version}" />
                <br/>

                    <div class="row">
                        <div class="col-sm-4">


                                    <div class="form-group ">
                                        <label class="control-label">Dirección Fecha</label>
                                        <input type="text" id="direccionFecha" name="direccionFecha" class="form-control" value="${configuracion.direccionFecha}" minlength="5" maxlength="20" required="" />
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Consecutivo Notas</label>
                                        <input type="text" id="consecutivoNotas" name="consecutivoNotas" class="form-control" value="${configuracion.consecutivoNotas}" required="" />
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label ">Días Recordatorio</label>
                                        <input type="text" id="diasRecordatorio" name="diasRecordatorio"
                                               class="form-control" value="${configuracion.diasRecordatorio}" maxlength="50" required="" />
                                    </div>

                                    <div class="form-group ">

                                        <label class="control-label">Días Gracia</label>
                                        <input type="text" id="diasGraciaCorte" name="diasGraciaCorte"
                                               class="form-control"
                                               value="${configuracion.diasGraciaCorte}" maxlength="50" required="" />
                                    </div>

                            <div class="form-group ">

                                <label class="control-label">Teléfono</label>
                                <input type="text" id="telefono" name="telefono"
                                       class="form-control"
                                       value="${configuracion.telefono}" maxlength="50" required="" />
                            </div>



                            <div class="form-group ">

                                <label class="control-label">Permiso IFT</label>
                                <input type="text" id="permisoIft" name="permisoIft"
                                       class="form-control"
                                       value="${configuracion.permisoIft}" maxlength="50" required="" />
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                <div class="col-md-6">
                                <label class="control-label">Editar notas</label>
                                <g:checkBox name="editarNota" class="form-control" value="${configuracion.editarNota}" />
                                </div>
                                <div class="col-md-6">
                                <label class="control-label">Cobro post pago</label>
                                <g:checkBox name="postPago" class="form-control" value="${configuracion.postPago}" />
                                </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">Secret Manual</label>
                                        <g:checkBox name="secretManual" class="form-control" value="${configuracion.secretManual}" />
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Actualizar Secret</label>
                                        <g:checkBox name="secretUpdate" class="form-control" value="${configuracion.secretUpdate}" />
                                    </div>
                                </div>
                            </div>






                        </div>
                        <div class="col-md-6">

                            <div class="form-group ">

                                <label class="control-label">Dirección</label>
                                <input type="text" id="direccion" name="direccion"
                                       class="form-control"
                                       value="${configuracion.direccion}" maxlength="100" required="" />
                            </div>

                            <div class="form-group ">

                                <label class="control-label">Código Postal</label>
                                <input type="text" id="codigoPostal" name="codigoPostal"
                                       class="form-control"
                                       value="${configuracion.codigoPostal}" maxlength="5" required="" />
                            </div>



                            <div class="form-group">
                                <label class="control-label">Mensaje corte</label>
                                <textarea rows="2" cols="2" id="mensajeCorte" name="mensajeCorte"
                                          class="form-control"
                                          maxlength="100" required="">${configuracion.mensajeCorte}</textarea>
                            </div>


                        <div class="form-group">
                            <label class="control-label">Mensaje recordatorio</label>
                            <textarea rows="2" cols="2" id="mensajeRecordatorio" name="mensajeRecordatorio"
                                      class="form-control"
                                      maxlength="100" required=""> ${configuracion.mensajeRecordatorio}
                            </textarea>
                        </div>

                            <div class="form-group">
                                <label class="control-label">Avisos</label>
                                <textarea rows="2" cols="2" id="aviso" name="aviso"
                                          class="form-control"
                                          maxlength="500" required=""> ${configuracion.aviso}
                                </textarea>
                            </div>

                            <div class="form-group ">

                                <label class="control-label">ID Dispositivo</label>
                                <input type="text" id="idDispositivo" name="idDispositivo"
                                       class="form-control"
                                       value="${configuracion.idDispositivo}" maxlength="50" required="" />
                            </div>

                            <div class="form-group ">

                                <label class="control-label">Key Dispositivo </label>
                                <textarea rows="2" cols="2" id="keyDispositivo" name="keyDispositivo"
                                       class="form-control" required="" >${configuracion.keyDispositivo}</textarea>
                            </div>

                        </div>



                    </div>

                <fieldset class="buttons">
                    <input class="btn btn-primary" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
            <br>
            <br>

            <div class="col-md-10">
                <g:form controller="historial" action="generarPagos">

                    Introduzca el año para el cual se generaran los pagos:

                    <input type="text" id="year" name="year" class="col-md-2">


                    <input class="btn btn-success" type="submit" value="${message(code: 'default.button.generar.pagos.label', default: 'Generar Pagos')}" />
                </g:form>
            </div>
            <br>
            <br>
        </div>
    </body>
</html>
