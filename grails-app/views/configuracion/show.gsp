<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'configuracion.label', default: 'Configuracion')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="show-configuracion" class="content scaffold-show" role="main">
            <h1>Parametros del Sistema</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="configuracion" />
            <g:form resource="${this.configuracion}" method="PUT">
                <fieldset class="buttons">
                    <input class="btn btn-primary" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
