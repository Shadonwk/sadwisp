<!doctype html>
<html>
<head>
    <meta name="layout" content="inicio"/>
    <title>SadWisp</title>
</head>
<body>


    <h3>Sistema de Administración para Wisp #SadWisp</h3>
    <br>
    <div class="row">
    <div class="col-md-6">
        <h4>Características agregadas</h4>


        <ul>
            <li>Agregar Mikrotiks</li>
            <li>Agregar planes Simple Queue especificos para cada Mikrotik</li>
            <li>Agregar planes PCQ especificos para cada Mikrotik</li>
            <li>Agregar planes PPPOE especificos para cada Mikrotik</li>
            <li>Dar de alta clientes</li>
            <li>Realizar envio de alta de cliente al mikrotik (evita crearlo manual)</li>
            <li>Genera historial de pagos automaticamente al dar de alta cliente</li>
            <li>Realiza corte de servicio al cliente al cumplirse la fecha de corte</li>
            <li>Realiza reactivación de cliente al registrar pago</li>
            <li>Impresión de recibo de pago</li>
            <li>Notificaciones de usuarios suspendidos</li>
        </ul>
    </div>
    <div class="col-md-6">
        <h4>Características por agregar</h4>
        <ul>
            <li>Envio de correos a usuarios </li>
            <li>Items de escritorio (por definir)</li>
            <li>Informes y graficas (por definir)</li>
        </ul>
    </div>
    </div>

<div class="row">
    <div class="col-md-6">
    <h4>Restricciones versión de prueba</h4>
    <ul>
        <li>Se pueden crear unicamente 1 Mikrotik</li>
        <li>Se pueden crear unicamente 10 planes</li>
        <li>Se pueden crear unicamente 100 clientes</li>
    </ul>
    </div>
</div>

</body>
</html>
