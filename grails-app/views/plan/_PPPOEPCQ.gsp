<div class="form-group col-md-4">
    <label class="control-label" for="addressList"><g:message code="plan.pppoe.adress.list"/></label>
    <g:textField name="addressList" class="form-control" maxlength="50" required="" value="${plan?.addressList}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="localAddress"><g:message code="plan.pppoe.local.adress"/></label>
    <g:textField name="localAddress" class="form-control" maxlength="50" required="" value="${plan?.localAddress}"/>
</div>


<div class="form-group col-md-4">
    <label class="control-label" for="remoteAddress"><g:message code="plan.pppoe.remote.adress"/></label>
    <g:select name="remoteAddress" class="form-control" id="remoteAddress"
              from="${poolList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              required="true"

    />
</div>


