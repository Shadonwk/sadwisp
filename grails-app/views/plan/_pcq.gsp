<div class="form-group col-md-4">
    <label class="control-label" for="pcqUpload"><g:message code="plan.pcq.upload.label"/></label>
    <g:select name="pcqUpload" class="form-control" id="pcqUpload"
              from="${uploadList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              required="true"

    />
</div>


<div class="form-group col-md-4">
    <label class="control-label" for="pcqDownload"><g:message code="plan.pcq.download.label"/></label>
    <g:select name="pcqDownload" class="form-control" id="pcqDownload"
              from="${downloadList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              required="true"
    />
</div>
