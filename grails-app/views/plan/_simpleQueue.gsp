<div class="form-group col-md-4">
    <label class="control-label" for="upload"><g:message code="plan.upload.label"/></label>
    <g:textField name="upload" class="form-control" maxlength="50" required="" value="${plan?.upload}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="uploadMedida"><g:message code="plan.upload.medida.label"/></label>
    <g:textField name="uploadMedida" class="form-control" maxlength="50" required="" value="${plan?.uploadMedida}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="download"><g:message code="plan.download.label"/></label>
    <g:textField name="download" class="form-control" maxlength="50" required="" value="${plan?.download}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="downloadMedida"><g:message code="plan.download.medida.label"/></label>
    <g:textField name="downloadMedida" class="form-control" maxlength="50" required="" value="${plan?.downloadMedida}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="burstLimit"><g:message code="plan.burst.limit.label"/></label>
    <g:textField name="burstLimit" class="form-control" maxlength="50" required="" value="${plan?.burstLimit}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="burstThreshold"><g:message code="plan.burst.threshold.label"/></label>
    <g:textField name="burstThreshold" class="form-control" maxlength="50" required="" value="${plan?.burstThreshold}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="burstTime"><g:message code="plan.burst.time.label"/></label>
    <g:textField name="burstTime" class="form-control" maxlength="50" required="" value="${plan?.burstTime}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="parent"><g:message code="plan.parent.label"/></label>
    <g:textField name="parent" class="form-control" maxlength="50" required="" value="${plan?.parent}"/>
</div>

