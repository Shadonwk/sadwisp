
<div class="form-group col-md-4">
    <label class="control-label" for="remoteAddress"><g:message code="plan.hotspot.user.profile"/></label>
    <g:select name="remoteAddress" class="form-control" id="remoteAddress"
              from="${profileList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              required="true"

    />
</div>


