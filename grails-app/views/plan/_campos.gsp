<div class="row">
    <hr>
    <div class="form-group col-md-4">
        <label class="control-label" for="nombre"><g:message code="plan.nombre.label"/></label>
        <g:textField name="nombre" class="form-control" maxlength="50" required="" value="${plan?.nombre}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="costo"><g:message code="plan.costo.label"/></label>
        <g:textField name="costo" class="form-control" maxlength="50" required="" value="${plan?.costo}"/>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label" for="mikrotik"><g:message code="cliente.mikrotik.label"/></label>
        <g:select name="mikrotik" class="form-control"
                  from="${mx.com.wisnet.sadwisp.Mikrotik.list()}"
                  optionKey="id"
                  required=""/>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label" for="tipo"><g:message code="plan.tipo.label"/></label>
        <g:select name="tipo" class="form-control"
                  from="['Simple Queue','PCQ', 'PPPOE', 'PPPOE PCQ', 'Hotspot']"
                  noSelection="${['':message(code:'plan.select.tipo.label')]}"
                  onchange="onTipoSelect()"
                  required=""/>
    </div>



</div>

<div id='response' class="row"></div>
