<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'plan.label', default: 'Plan')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="show-plan" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <br>
            <table class="table table-striped table-bordered table-condensed table-hover">
                <tbody>
                <tr>
                    <th class="span3"><g:message code="plan.mikrotik.label" default="Mikrotik"/></th>
                    <td><g:fieldValue bean="${plan}" field="mikrotik" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="plan.tipo.label" default="Plan"/></th>
                    <td><g:fieldValue bean="${plan}" field="tipo" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="plan.nombre.label" default="Nombre"/></th>
                    <td><g:fieldValue bean="${plan}" field="nombre" /></td>

                </tr>

                <tr>
                    <th class="span3"><g:message code="plan.costo.label" default="Costo"/></th>
                    <td><g:fieldValue bean="${plan}" field="costo" /></td>
                </tr>

                <g:if test="${plan.tipo.equals('PCQ')}">
                    <tr>
                        <th class="span3"><g:message code="plan.pcq.upload.label" default="Plan"/></th>
                        <td><g:fieldValue bean="${plan}" field="pcqUpload" /></td>
                    </tr>

                    <tr>
                        <th class="span3"><g:message code="plan.pcq.download.label" default="Plan"/></th>
                        <td><g:fieldValue bean="${plan}" field="pcqDownload" /></td>
                    </tr>
                </g:if>



                <g:if test="${plan.tipo.equals('PPPOE')}">
                        <tr>
                            <th class="span3"><g:message code="plan.pppoe.local.adress" default="Plan"/></th>
                            <td> ${plan.localAddress}</td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="plan.pppoe.remote.adress" default="Plan"/></th>
                            <td> ${plan.remoteAddress}</td>
                        </tr>

                        <tr>
                            <th class="span3"><g:message code="plan.upload.label" default="Plan"/></th>
                            <td>${plan.upload} ${plan.uploadMedida}</td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="plan.download.label" default="Plan"/></th>
                            <td>${plan.download} ${plan.downloadMedida}</td>
                        </tr>
                </g:if>

                <g:if test="${plan.tipo.equals('PPPOE PCQ')}">
                        <tr>
                            <th class="span3"><g:message code="plan.pppoe.local.adress" default="Plan"/></th>
                            <td> ${plan.localAddress}</td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="plan.pppoe.remote.adress" default="Plan"/></th>
                            <td> ${plan.remoteAddress}</td>
                        </tr>
                        <tr>
                            <th class="span3"><g:message code="plan.pppoe.adress.list" default="Plan"/></th>
                            <td> ${plan.addressList}</td>
                        </tr>
                </g:if>

                <g:if test="${plan.tipo.equals('Simple Queue')}">
                    <tr>
                        <th class="span3"><g:message code="plan.burst.limit.label" default="Plan"/></th>
                        <td>${plan.burstLimit} ${plan.downloadMedida}</td>
                    </tr>
                    <tr>
                        <th class="span3"><g:message code="plan.burst.threshold.label" default="Plan"/></th>
                        <td>${plan.burstThreshold} ${plan.downloadMedida}</td>
                    </tr>

                    <tr>
                        <th class="span3"><g:message code="plan.burst.time.label" default="Plan"/></th>
                        <td>${plan.burstTime}</td>
                    </tr>
                    <tr>
                        <th class="span3"><g:message code="plan.parent.label" default="Plan"/></th>
                        <td>${plan.parent}</td>
                    </tr>
                    <tr>
                        <th class="span3"><g:message code="plan.upload.label" default="Plan"/></th>
                        <td>${plan.upload} ${plan.uploadMedida}</td>
                    </tr>
                    <tr>
                        <th class="span3"><g:message code="plan.download.label" default="Plan"/></th>
                        <td>${plan.download} ${plan.downloadMedida}</td>
                    </tr>
                </g:if>

                </tbody>
            </table>


            <g:form resource="${this.plan}" method="DELETE">
                <fieldset class="buttons">
                    <g:if test="${plan.tipo.equals('PPPOE')||plan.tipo.equals('PPPOE PCQ')}">
                        <g:link class="btn btn-success" action="sendMikrotik" resource="${this.plan}"><g:message code="cliente.new.queue" default="Enviar Mikrotik" /></g:link>
                    </g:if>
                    <g:link class="btn btn-primary" action="edit" resource="${this.plan}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
