<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'plan.label', default: 'Plan')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div class="nav" role="navigation">

            <g:link class="btn btn-primary btn-block col-md-2 col-sm-2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>

        </div>
    <br>
        <div id="list-plan" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <br>

            <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Costo</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${planList}" var="item">
                    <tr>

                        <td>

                            <g:link action="show" id="${item.id}">
                                ${item}
                            </g:link>
                        </td>
                        <td><g:fieldValue bean="${item}" field="tipo" /></td>
                        <td><g:fieldValue bean="${item}" field="costo" /></td>

                    </tr>
                </g:each>
                </tbody>
            </table>


            <div class="pagination">
                <g:paginate total="${planCount ?: 0}" />
            </div>
        </div>
    </body>
</html>