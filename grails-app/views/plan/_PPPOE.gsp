<div class="form-group col-md-4">
    <label class="control-label" for="upload"><g:message code="plan.upload.label"/></label>
    <g:textField name="upload" class="form-control" maxlength="50" required="" value="${plan?.upload}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="uploadMedida"><g:message code="plan.upload.medida.label"/></label>
    <g:textField name="uploadMedida" class="form-control" maxlength="50" required="" value="${plan?.uploadMedida}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="download"><g:message code="plan.download.label"/></label>
    <g:textField name="download" class="form-control" maxlength="50" required="" value="${plan?.download}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="downloadMedida"><g:message code="plan.download.medida.label"/></label>
    <g:textField name="downloadMedida" class="form-control" maxlength="50" required="" value="${plan?.downloadMedida}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="localAddress"><g:message code="plan.pppoe.local.adress"/></label>
    <g:textField name="localAddress" class="form-control" maxlength="50" required="" value="${plan?.localAddress}"/>
</div>


<div class="form-group col-md-4">
    <label class="control-label" for="remoteAddress"><g:message code="plan.pppoe.remote.adress"/></label>
    <g:select name="remoteAddress" class="form-control" id="remoteAddress"
              from="${poolList}"
              noSelection="${['':message(code:'plan.pcq.select.label')]}"
              required="true"

    />
</div>


