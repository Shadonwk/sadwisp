<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'mikrotik.label', default: 'Mikrotik')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>


        <div id="show-mikrotik" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <br>
            <table class="table table-striped table-bordered table-condensed table-hover">
                <tbody>
                <tr>
                    <th class="span3"><g:message code="mikrotik.nombre.label" default="Nombre"/></th>
                    <td><g:fieldValue bean="${mikrotik}" field="nombre" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="mikrotik.ip.mikrotik.label" default="Ip"/></th>
                    <td><g:fieldValue bean="${mikrotik}" field="ipMikrotik" /></td>
                </tr>

                <tr>
                    <th class="span3"><g:message code="mikrotik.puerto.label" default="Puerto"/></th>
                    <td><g:fieldValue bean="${mikrotik}" field="puerto" /></td>
                </tr>

                <tr>
                    <th class="span3"><g:message code="mikrotik.usuario.label" default="Usuario"/></th>
                    <td><g:fieldValue bean="${mikrotik}" field="usuarioMikrotik" /></td>
                </tr>

                <tr>
                    <th class="span3"><g:message code="mikrotik.modelo.label" default="Usuario"/></th>
                    <td>${detalles}</td>
                </tr>


                </tbody>
            </table>


            <g:form resource="${this.mikrotik}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="btn btn-success" action="importarClientes" resource="${this.mikrotik}"><g:message code="mikrotik.importar.label" default="Importar Queues" /></g:link>
                    <g:link class="btn btn-success" action="importarPPPOE" resource="${this.mikrotik}"><g:message code="mikrotik.importar.pppoe.label" default="Importar PPPoE" /></g:link>
                    <g:link class="btn btn-primary"  action="edit" resource="${this.mikrotik}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="btn btn-danger"  type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
