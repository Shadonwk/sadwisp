<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'mikrotik.label', default: 'Mikrotik')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="edit-mikrotik" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.mikrotik}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.mikrotik}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.mikrotik}" method="PUT">
                <g:hiddenField name="version" value="${this.mikrotik?.version}" />

                <div class="row">
                    <div class="col-sm-4">

                          <div class="form-group ">
                              <label class="control-label">Nombre</label>
                              <input type="text" id="nombre" name="nombre" class="form-control" value="${mikrotik.nombre}" minlength="5" maxlength="20" required="" />
                          </div>

                        <div class="form-group">
                            <label class="control-label">IP</label>
                            <input type="text" id="ipMikrotik" name="ipMikrotik" class="form-control" value="${mikrotik.ipMikrotik}" required="" />
                        </div>

                        <div class="form-group ">
                            <label class="control-label ">Puerto</label>
                            <input type="text" id="puerto" name="puerto"
                                   class="form-control" value="${mikrotik.puerto}" maxlength="50" required="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">Usuario</label>
                            <input type="text" id="usuarioMikrotik" name="usuarioMikrotik" class="form-control" value="${mikrotik.usuarioMikrotik}" required="" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <g:passwordField name="passwordMikrotik" class="form-control" maxlength="50" required="" value="${mikrotik.passwordMikrotik}"/>
                        </div>


                    </div>
                </div>


                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
