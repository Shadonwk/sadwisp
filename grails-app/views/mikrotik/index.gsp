<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'mikrotik.label', default: 'Mikrotik')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div class="nav" role="navigation">

                <g:link class="btn btn-primary btn-block col-md-2 col-sm-2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>

        </div>
    <br>
        <div id="list-mikrotik" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <br>

            <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>IP</th>
                    <th>Usuario</th>
                    <th>Puerto</th>
                    <th>Clientes</th>
                    <th>Estatus</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${mikrotikList}" var="item">
                    <tr>

                        <td>
                            <g:link action="show" id="${item.id}">
                                <g:fieldValue bean="${item}" field="nombre" />
                            </g:link>
                        </td>
                        <td><g:fieldValue bean="${item}" field="ipMikrotik" /></td>
                        <td><g:fieldValue bean="${item}" field="usuarioMikrotik" /></td>
                        <td><g:fieldValue bean="${item}" field="puerto" /></td>
                        <td>${item.clientes()}</td>
                        <td>${item.status()}</td>



                    </tr>
                </g:each>
                </tbody>
            </table>



            <div class="pagination">
                <g:paginate total="${mikrotikCount ?: 0}" />
            </div>
        </div>
    </body>
</html>