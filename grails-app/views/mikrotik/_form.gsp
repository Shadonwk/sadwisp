<div class="row">
    <hr>
    <div class="form-group col-md-4">
        <label class="control-label" for="nombre"><g:message code="mikrotik.nombre.label"/></label>
        <g:textField name="nombre" class="form-control" maxlength="50" required="" value="${mikrotik.nombre}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="ipMikrotik"><g:message code="mikrotik.ip.mikrotik.label"/></label>
        <g:textField name="ipMikrotik" class="form-control" maxlength="50" required="" value="${mikrotik.ipMikrotik}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="puerto"><g:message code="mikrotik.puerto.label"/></label>
        <g:textField name="puerto" class="form-control" maxlength="50" required="" value="${mikrotik.puerto}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="usuarioMikrotik"><g:message code="mikrotik.usuario.label"/></label>
        <g:textField name="usuarioMikrotik" class="form-control" maxlength="50" required="" value="${mikrotik.usuarioMikrotik}"/>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label" for="passwordMikrotik"><g:message code="mikrotik.password.label"/></label>
        <g:passwordField name="passwordMikrotik" class="form-control" maxlength="50" required="" value="${mikrotik.passwordMikrotik}"/>
    </div>

</div>

