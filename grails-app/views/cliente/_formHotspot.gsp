<div class="row">
<hr>
<div class="form-group col-md-4">
    <label class="control-label" for="nombre"><g:message code="cliente.nombre.label"/></label>
    <g:textField name="nombre" class="form-control" maxlength="50" required="" value="${cliente.nombre}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="apellidoPaterno"><g:message code="cliente.paterno.label"/></label>
    <g:textField name="apellidoPaterno" class="form-control" maxlength="50" required="" value="${cliente.apellidoPaterno}"/>
</div>
<div class="form-group col-md-4">
    <label class="control-label" for="apellidoMaterno"><g:message code="cliente.materno.label"/></label>
    <g:textField name="apellidoMaterno" class="form-control" maxlength="50" required="" value="${cliente.apellidoMaterno}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="email"><g:message code="cliente.email.label"/></label>
    <g:textField name="email" class="form-control" maxlength="50"  value="${cliente.email}"/>
</div>


<div class="form-group col-md-4">
    <label class="control-label" for="telefono"><g:message code="cliente.telefono.label"/></label>
    <g:textField name="telefono" class="form-control" maxlength="20" value="${cliente.telefono}"/>
</div>

<div class="form-group col-md-4">
    <label class="control-label" for="direccion"><g:message code="cliente.direccion.label"/></label>
    <g:textField name="direccion" class="form-control" maxlength="200" required="" value="${cliente.direccion}"/>
</div>

    <div class="form-group col-md-4">
        <label class="control-label" for="diaCorte"><g:message code="cliente.dia.corte.label"/></label>
        <g:textField name="diaCorte" class="form-control" maxlength="50" required="" value="${cliente.diaCorte}"/>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label" for="mikrotik"><g:message code="cliente.mikrotik.label"/></label>
        <g:select name="mikrotik" class="form-control"
                  from="${mx.com.wisnet.sadwisp.Mikrotik.list()}"
                  optionKey="id"
                  required=""/>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label" for="plan"><g:message code="cliente.plan.label"/></label>

        <g:select name="plan" class="form-control"
                  from="${mx.com.wisnet.sadwisp.Plan.findAllByTipo('Hotspot')}"
                    value="${cliente?.plan?.id}"
                  optionKey="id"
                  required=""/>
    </div>


    <div class="form-group col-md-4">
        <label class="control-label" for="comentarios"><g:message code="cliente.comentarios.label"/></label>
        <g:textField name="comentarios" class="form-control" maxlength="220" value="${cliente.comentarios}"/>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label" for="macAntena"><g:message code="cliente.mac.label"/></label>
        <g:textField name="macAntena" class="form-control" required="true" maxlength="50"  value="${cliente.macAntena}"/>
    </div>



    <div class="form-group col-md-4">
        <label class="control-label" for="primerMes"><g:message code="cliente.primer.mes.gratis.label"/></label>
        <g:checkBox name="primerMes" class="form-control" value="true"/>
    </div>

</div>

