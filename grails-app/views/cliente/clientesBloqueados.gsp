<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'cliente.label', default: 'Cliente')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div class="nav" role="navigation">

            <g:link class="btn btn-primary btn-block col-sm-2 col-md-2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>


            <div class="col-md-4 offset-6 align-self-md-end">
            <g:form name="searchForm"
            class="form-search form-inline"
            url="[controller:'cliente', action:'buscar']"
            before="limpiarParaBusqueda()"
            update="main-detail-div">
            <div class="input-append">
            <g:textField id="query" name="query" placeholder="Nombre de usuario" class="form-control search-query"/>
            <g:submitButton name="buscar" value="${message(code: 'accion.buscar',default: 'Buscar')}"
                            class="btn btn-primary"/>
            </div>
            </g:form>
            </div>
        </div>
    <br>
        <div id="list-cliente" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <br>

            <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Estatus</th>
                    <th>Queue</th>
                    <th>IP</th>
                    <th>Plan</th>
                    <th>Día corte</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${clienteList}" var="item">
                    <tr>

                        <td>
                            <g:link action="show" id="${item.id}">
                                ${item.getNombreCompleto()}</td>
                            </g:link>
                        <td>
                            <g:link controller="historial" action="show" id="${item.id}">
                            ${item.statusServicio}
                            </g:link>
                        </td>
                        <td><g:fieldValue bean="${item}" field="name" /></td>
                        <td><g:fieldValue bean="${item}" field="ip" /></td>
                        <td><g:fieldValue bean="${item}" field="plan" /></td>
                        <td><g:fieldValue bean="${item}" field="diaCorte" /></td>


                    </tr>
                </g:each>
                </tbody>
            </table>



            <div class="pager mx-auto" align="center">
                <g:paginate total="${clienteCount ?: 0}" />
            </div>
        </div>
    </body>
</html>