<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'cliente.label', default: 'Cliente')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>


        <div id="show-cliente" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <hr>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:if test="${flash.errorMessage}">
                <div class="errors" role="status">${flash.errorMessage}</div>
            </g:if>

            <table class="table table-striped table-bordered table-condensed table-hover">
                <tbody>
                <tr>
                    <th class="span3"><g:message code="cliente.usuario.label" default="Nombre"/></th>
                    <td><g:fieldValue bean="${cliente}" field="nombreCompleto" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.email.label" default="Email"/></th>
                    <td><g:fieldValue bean="${cliente}" field="email" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.telefono.label" default="Telefono"/></th>
                    <td>${cliente.telefono}</td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.direccion.label" default="Direcciòn"/></th>
                    <td><g:fieldValue bean="${cliente}" field="direccion" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.plan.label" default="Plan"/></th>
                    <td><g:fieldValue bean="${cliente}" field="plan" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.fechaAlta.label" default="Dìa de pago"/></th>
                    <td><g:fieldValue bean="${cliente}" field="diaCorte" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.queue.label" default="Nombre queue"/></th>
                    <td><g:fieldValue bean="${cliente}" field="name" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.ip.label" default="IP"/></th>
                    <td><g:fieldValue bean="${cliente}" field="ip" /></td>
                </tr>

                <tr>
                    <th class="span3"><g:message code="usuario.mac.label" default="Mac"/></th>
                    <td><g:fieldValue bean="${cliente}" field="macAntena" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.mikrotik.label" default="Mikrotik asignado"/></th>
                    <td><g:fieldValue bean="${cliente}" field="mikrotik" /></td>
                </tr>
                <tr>
                    <th class="span3"><g:message code="usuario.caja.label" default="Caja"/></th>
                    <td><g:fieldValue bean="${cliente}" field="caja" /></td>
                </tr>

                <tr>
                    <th>Ubicación </th>
                    <td>
                        <g:if test="${cliente.latitud!=null}">
                            <a href="https://www.google.com/maps/place/${cliente.latitud},${cliente.longitud}">Ver mapa</a>
                        </g:if>
                        <g:else>Sin ubicación</g:else>
                    </td>
                </tr>


                </tbody>
            </table>


        <sec:ifAllGranted roles="ROLE_ADMINISTRADOR">
            <g:form resource="${this.cliente}" method="DELETE">
                <fieldset class="buttons">
                    <br>
                    <g:link class="btn btn-success" action="sendMikrotik" resource="${this.cliente}"><g:message code="cliente.new.queue" default="Enviar Mikrotik" /></g:link>
                    <g:link class="btn btn-primary"  action="edit" resource="${this.cliente}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    <br>
                </fieldset>
            </g:form>
        </sec:ifAllGranted>




        </div>
    </body>

</html>
