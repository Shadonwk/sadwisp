<!doctype html>
<html>
<head>
    <meta name="layout" content="inicio"/>
    <title>SadWisp</title>
    <style>


    #sigma-container { width:300px; height:300px; background-color:#FFFFFF; color: #fff!important;}
    #sigma-mouse{
        background: transparent;
    }

    </style>
</head>
<body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js"></script>

<h2>Dashboard</h2>
<hr>
<br>
<div class="row">
    <div class="col-sm-6">
        <div class=" card border-success mb-3">
            <div class="card-header bg-success border-success text-white" >
                <h5><i class="fas fa-money-check-alt">  P a g o s</i></h5>
            </div>
            <div class="card-body ">
                <h6>Pagos hoy: ${pagosHoy}</h6>
                <h6>Pagos en el mes: ${pagosMes}</h6>
                <br>
                <div class="col-sm-9">
                    <canvas id="grafica"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card border-primary">
            <div class="card-header bg-primary border-primary text-white">
                <h5><i class="fas fa-user">  C l i e n t e s</i></h5>
            </div>
            <div class="card-body">
                <h6>Clientes en linea: ${clientesLinea}</h6>
                <h6>Clientes suspendidos: ${clientesSuspendidos}</h6>
                <br>
                <div class="col-sm-10">
                    <br>
                    <canvas id="grafica2"></canvas>
                    <br>
                </div>
            </div>
        </div>
    </div>

</div>
<br>
<br>


<br>
<br>

<script>

    getGraficaPagos(${pagadoEnMes}, ${porCobrarEnMes});
    getGraficaNuevosClientes(${clientesNuevos.semana1},${clientesNuevos.semana2},${clientesNuevos.semana3},${clientesNuevos.semana4});
</script>

</body>
</html>
