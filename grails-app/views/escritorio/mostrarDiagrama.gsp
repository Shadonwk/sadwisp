<!doctype html>
<html>
<head>
    <meta name="layout" content="inicio"/>
    <title>SadWisp</title>
    <style>


    #sigma-container { width:300px; height:300px; background-color:#FFFFFF; color: #fff!important;}
    #sigma-mouse{
        background: transparent;
    }

    </style>
</head>
<body>

<h3>Dashboard</h3>
<hr>
<br>
<div class="row">
    <div class="col-md-6"><i class="fas fa-money-check-alt">  P a g o s</i>
    <br><br><br>Pagos hoy: ${pagosHoy}
        <br><br>Pagos en el mes: ${pagosMes}
    </div>
    <div class="col-md-6"><i class="fas fa-user">  C l i e n t e s</i>
    <br><br><br>Clientes en linea: ${clientesLinea}
        <br><br>Clientes suspendidos: ${clientesSuspendidos}
    </div>
</div>
<br>
<br>

<div class="row">
    <div class="col-md-5">
    <br>
    <br>
    <g:each in="${resumen}" var="item">
        <br>
        ${item.key} ${item.value}
    </g:each>

    </div>

    <div class="col-md-6" id="sigma-container">


    </div>

</div>
<br>
<br>


<script>
    var s = new sigma(
        {
            renderer: {
                container: document.getElementById('sigma-container'),
                type: 'canvas'
            },
            settings: {
                labelColor: 'node',
                labelThreshold: 5,
                minEdgeSize: 0.1,
                maxEdgeSize: 2,
                minNodeSize: 5,
                maxNodeSize: 5,
            }
        }
    );

    var graph = {
        nodes: [
            { id: "n0", label: "PON01",  x: 0, y: 0, size: 3, color: '#00BCD4' },
            { id: "n1", label: "Caja 01", x: 2, y: 1, size: 2, color: '#002ed4' },
            { id: "n2", label: "Caja 02", x: 1, y: 2, size: 1, color: '#002ed4' },
            { id: "n3", label: "Cliente 1", x: 5, y: 0, size: 3, color: '#40d400' },
            { id: "n4", label: "Cliente 2", x: 4, y: 1, size: 2, color: '#40d400' },
            { id: "n5", label: "Cliente 3", x: 3, y: 3, size: 1, color: '#40d400' }
        ],
        edges: [
            { id: "e0", source: "n0", target: "n1",  color: '#545454', type:'line'},
            { id: "e1", source: "n0", target: "n2", color: '#545454', type:'line'},
            { id: "e2", source: "n1", target: "n3", color: '#545454', type:'line'},
            { id: "e3", source: "n1", target: "n4", color: '#545454', type:'line'},
            { id: "e4", source: "n2", target: "n5", color: '#545454', type:'line'},



        ]
    }




    s.graph.read(graph);
    s.refresh();

</script>

</body>
</html>
