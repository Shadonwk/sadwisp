<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="inicio" />
        <g:set var="entityName" value="${message(code: 'notificacion.label', default: 'Notificacion')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="list-notificacion" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <br>

            <table id="list-portafolios" class="table table-striped table-bordered table-condensed table-hover tablesorter">
                <thead>
                <tr>
                    <th>Destinatario</th>
                    <th>Mensaje</th>
                    <th>Fecha Notificación</th>
                    <th>Acciones</th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${notificacionList}" var="item">
                        <td><g:fieldValue bean="${item}" field="destinatario.nombreCompleto" /></td>
                        <td><g:fieldValue bean="${item}" field="mensaje" /></td>
                        <td><g:fieldValue bean="${item}" field="fechaNotificacion" /></td>
                        <td><g:form resource="${item}" method="DELETE">
                            <fieldset class="buttons">
                            <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </fieldset>
                        </g:form></td>


                    </tr>
                </g:each>
                </tbody>
            </table>


            <div class="pagination">
                <g:paginate total="${notificacionCount ?: 0}" />
            </div>
        </div>
    </body>
</html>