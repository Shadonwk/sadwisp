package mx.com.sadwisp.cliente

import me.legrange.mikrotik.ApiConnection
import mx.com.wisnet.sadwisp.Cliente
import mx.com.wisnet.sadwisp.Configuracion
import mx.com.wisnet.sadwisp.Historial
import mx.com.wisnet.sadwisp.Notificacion
import mx.com.wisnet.sadwisp.dto.MessageSMSDTO
import org.grails.web.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

import java.time.LocalDate

class EnviarRecordatorioPagoJob {
    static triggers = {

      //cron name: 'enviarMensajePago', cronExpression: "00 45 * * * ?"
      cron name: 'enviarMensajePago', cronExpression: "00 00 11,17 * * ?"
    }

    def execute() {
        def configuracion = Configuracion.get(1)
        def sms = configuracion.mensajeRecordatorio
        def idDispositivo = configuracion.idDispositivo
        def keyDispositivo = configuracion.keyDispositivo


        LocalDate fecha = LocalDate.now();
        def dia = fecha.getDayOfMonth()
        def diasRecordatorio = configuracion.diasRecordatorio
        def fechaACortar = fecha.getDayOfMonth()

        def clientesPotenciales = []
        diasRecordatorio.times {
            println "enviar mensaje fecha: $fechaACortar menos  $it"
            def clientes = Cliente.findAllByDiaCorteAndStatusServicio(fechaACortar, "EN_LINEA")
            if (!clientes.isEmpty()){
                clientes.each {
                    clientesPotenciales.add(it)
                }
            }

            fechaACortar--
        }

        //obtener los clientes que aún no han pagado

        def clientes = []
        clientesPotenciales.each {
            def historial = Historial.findByMesAndAnioAndCliente(fecha.monthValue, fecha.year, it)
            if (!historial.statusPago) {
                clientes.add(it)
            }
        }

        println clientes
        //se obtiene la conexiòn al mikrotik


        clientes.each {
            def mensajeConfecha = sms + " $it.diaCorte"
            sendRecordatorioServiceMessage(it, mensajeConfecha, idDispositivo, keyDispositivo)
        }

        println "termina tarea envio de recordatorios con exito"



}
    def sendRecordatorioServiceMessage(Cliente cliente, String sms, String idDevice, String keyDevice){
        HttpHeaders headers = jsonMediaType(keyDevice)
        RestTemplate rest = newRestTemplate()

        if(null!=cliente.telefono && !'NA'.equals(idDevice)) {
            def url = "https://smsgateway.me/api/v4/message/send"
            def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: idDevice)
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "103178")
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "104360")
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "104389")
            def json = new JSONArray()
            json.add(message)

            HttpEntity<JSONArray> request = new HttpEntity<>(json, headers)
            rest.postForObject(url, request, JSONArray.class)
            println "Mensaje de recordatorio enviado a $cliente.nombreCompleto"
        }

    }

    private static HttpHeaders jsonMediaType(String keyDispositivo) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.setAccept([MediaType.APPLICATION_JSON])
        headers.set('Authorization', keyDispositivo)
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUzODg3ODE1OSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYyMjk5LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.aYWn57tRSROlwLJ5v8Xahr_yv0OyGh5eXhGp8qbk3yk')
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0MDk0NTk3MCwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYzMzUyLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.QgcvcIxbJ-KYSU4O_Nq3OBFjcfvv5kUDu8PthD7lRTs')
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0MTAxMTI3MiwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYzMzg5LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0._-r8U_iy9PDa9PqYrnS7iwpx2ca4TBBFBpFjI7ebOhE')
        headers
    }

    private static RestTemplate newRestTemplate() {
        new RestTemplate(getClientHttpRequestFactory())
    }

    private static ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory()
        clientHttpRequestFactory.setConnectTimeout(2000)
        clientHttpRequestFactory.setConnectionRequestTimeout(2000)
        clientHttpRequestFactory.setReadTimeout(2000)
        return clientHttpRequestFactory
    }
}
