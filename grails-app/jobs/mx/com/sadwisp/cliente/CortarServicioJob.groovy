package mx.com.sadwisp.cliente

import me.legrange.mikrotik.ApiConnection
import mx.com.wisnet.sadwisp.Cliente
import mx.com.wisnet.sadwisp.Configuracion
import mx.com.wisnet.sadwisp.Historial
import mx.com.wisnet.sadwisp.Notificacion
import mx.com.wisnet.sadwisp.dto.MessageSMSDTO
import org.grails.web.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

import java.time.LocalDate
import java.time.LocalTime

class CortarServicioJob {

    def clienteService
    ApiConnection con
    def notificacionService
    def parametrosService
    def usuarioService


    static triggers = {
        //cron name: 'cortarServicio', cronExpression: "00 45 0/4 * * ?"
        cron name: 'cortarServicio', cronExpression: "00 01 * * * ?"
    }

    def execute() {
        // execute job

        println("inicia a cortar el servicio " + LocalDate.now() + " a las :" + LocalTime.now())

        def configuracion = Configuracion.get(1)
        def sms = configuracion.mensajeCorte
        def idDispositivo = configuracion.idDispositivo
        def keyDispositivo = configuracion.keyDispositivo

        LocalDate fecha = LocalDate.now();


        def dia = fecha.getDayOfMonth()
        if(configuracion.isPostPago()){
            fecha = fecha.minusMonths(1)
        }
        def diasGracia = configuracion.diasGraciaCorte
        def fechaACortar = fecha.minusDays(diasGracia)
        println "corte servicio fecha: $dia días de gracias $diasGracia día a cortar $fechaACortar"

        //obtener los clientes que se les vence el servicio
        def clientesPotenciales = Cliente.findAllByDiaCorteAndStatusServicio(fechaACortar.getDayOfMonth(), "EN_LINEA")

        def clientes= []
        clientesPotenciales.each {
            def historial = Historial.findByMesAndAnioAndCliente(fechaACortar.monthValue, fecha.year, it)
            if(historial!= null && !historial.statusPago){
                clientes.add(it)
            }
        }

        def clientes2 = clientesPotenciales.findAll{ cliente ->
            Historial.findByMesAndAnioAndCliente(fechaACortar.monthValue, fecha.year, cliente)?.statusPago == false
        }.collect { it }

        println "clientes a suspender servicio:"
        println clientes
        //se obtiene la conexiòn al mikrotik


            clientes.each {
                try {
                def conexion = it.mikrotik//parametrosService.getParametrosMikrotik(it.cuenta)
                con = ApiConnection.connect(conexion.ipMikrotik); // connect to router
                //con = ApiConnection.connect(SSLSocketFactory.getDefault(), conexion.ipMikrotik, ApiConnection.DEFAULT_TLS_PORT, ApiConnection.DEFAULT_CONNECTION_TIMEOUT);
                con.login(conexion.usuarioMikrotik,conexion.passwordMikrotik); // log in to router
                println "se realizo la conexiòn, cortando el servicio ip "+ it.ip

                //buscar la regla en el mikrotik
                def regla =con.execute("/ip/firewall/filter/print where src-address-list='MOROSOS'");

                if(regla.isEmpty()){
                    //agregar la regla
                    println "agregando la regla"
                    con.execute("/ip/firewall/filter/add chain=forward action=drop src-address-list=MOROSOS");
                }

                println  "El tipo de cliente es: $it.plan.tipo"
                print " El cliente  es: $it.name"
                print " mikrotik asignado $it.mikrotik"
                if(it.plan.tipo.equals('PPPOE')||it.plan.tipo.equals('PPPOE PCQ')){
                    //Buscar el secret e inhabilitarlo

                    con.execute("/ppp/secret/disable numbers='$it.name'")
                    //Buscar la conexion activa
                    def item = con.execute("/ppp/active/print where name='$it.name'")
                    println item
                    //Si tiene una conexion activa eliminarla....
                    if(!item.isEmpty()){
                        def idEliminar = item.get(0).get(".id")
                        println "id a eliminar" + idEliminar
                        //Borrar la conexion activa
                        con.execute("/ppp/active/remove .id='$idEliminar'")
                    }


                }else if(it.plan.tipo.equals('Simple Queue') || (it.plan.tipo.equals('PCQ'))){
                    //Bloquea por IP
                    //buscar sino existe la ip en la lista

                    def ipLista =con.execute("/ip/firewall/address-list/print where address="+it.ip+" and list='MOROSOS'");

                    if(ipLista.isEmpty()){
                        def rs1 =con.execute("/ip/firewall/address-list/add address=" +it.ip+" list=MOROSOS");

                    }else{
                        println "ya existe la ip en la lista no hacer nada..."
                    }
                }else {
                    //para los hotspot
                    con.execute("/ip/hotspot/user/disable numbers='$it.macAntena'")

                    //Buscar la conexion activa
                    def item = con.execute("/ip/hotspot/active/print where user='$it.macAntena'")
                    println item
                    //Si tiene una conexion activa eliminarla....
                    if(!item.isEmpty()){
                        def idEliminar = item.get(0).get(".id")
                        println "id a eliminar" + idEliminar
                        //Borrar la conexion activa
                        con.execute("/ip/hotspot/active/remove .id='$idEliminar'")
                    }
                }

                //clienteService.guardarSuspendido(it.id)
                it.statusServicio="SUSPENDIDO"
                it.save(flush:true)
                //notificacionService.notificarSuspecionServicio(it.id)
                con.close()
                //sendSuspendServiceMessage(it, sms, idDispositivo,keyDispositivo)
                }catch (Exception e){
                    println "no se pudo realizar la conexiòn al mikrotik" + e.printStackTrace()
                    //e.printStackTrace()
                    //TODO agregar notificacion de error
                    //notificacionService.notificarErrorEnConexion(cliente.id)
                }
            }



        println "termina tarea corte de servicio con exito"
    }


    def sendSuspendServiceMessage(Cliente cliente, String sms, String idDispositivo, String keyDispositivo){
        HttpHeaders headers = jsonMediaType(keyDispositivo)
        RestTemplate rest = newRestTemplate()

        def mensaje = "Servicio suspendido sin notificación SMS"

        if(null!=cliente.telefono && !'NA'.equals(idDispositivo)) {
            def url = "https://smsgateway.me/api/v4/message/send"
            def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: idDispositivo)
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "103178")
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "104360")
            //def message = new MessageSMSDTO(phone_number: cliente.telefono, message: sms, device_id: "104389")
            def json = new JSONArray()
            json.add(message)

            HttpEntity<JSONArray> request = new HttpEntity<>(json, headers)
            rest.postForObject(url, request, JSONArray.class)
            mensaje = "Servicio suspendido con notificación SMS"
        }

        def notificacion = new Notificacion()
        notificacion.mensaje = mensaje
        notificacion.destinatario = cliente
        notificacion.fechaNotificacion = new Date()
        notificacionService.save(notificacion)


    }

    private static HttpHeaders jsonMediaType(String keyDispositivo) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.setAccept([MediaType.APPLICATION_JSON])
        headers.set('Authorization', keyDispositivo)
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTUzODg3ODE1OSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYyMjk5LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.aYWn57tRSROlwLJ5v8Xahr_yv0OyGh5eXhGp8qbk3yk')
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0MDk0NTk3MCwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYzMzUyLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.QgcvcIxbJ-KYSU4O_Nq3OBFjcfvv5kUDu8PthD7lRTs')
        //headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0MTAxMTI3MiwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYzMzg5LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0._-r8U_iy9PDa9PqYrnS7iwpx2ca4TBBFBpFjI7ebOhE')
        headers
    }

    private static RestTemplate newRestTemplate() {
        new RestTemplate(getClientHttpRequestFactory())
    }

    private static ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory()
        clientHttpRequestFactory.setConnectTimeout(2000)
        clientHttpRequestFactory.setConnectionRequestTimeout(2000)
        clientHttpRequestFactory.setReadTimeout(2000)
        return clientHttpRequestFactory
    }
}
