package mx.com.sadwisp.cliente

import mx.com.wisnet.sadwisp.Historial
import org.joda.time.DateTime

class AgregarRegistroHistorialJob {


    def clienteService
    def historialService
    def mikrotikHelperService

    static triggers = {
        //simple repeatInterval: 5000l // execute job once in 5 seconds
        cron name: 'agregarHistorial', cronExpression: "00 35 * * * ?"
    }

    def execute() {
        // execute job
        log.info "Inicia ejecución de la tarea que agrega nuevo historial en cada cliente "
        def clientes = clienteService.list()
        DateTime fecha = new DateTime();
        def mes = fecha.getMonthOfYear()
        def anio = fecha.getYear()
        log.debug " el mes es: $mes del año: $anio"
        clientes.each {
            mikrotikHelperService.buscarRegistroClienteAndMesAndAnio(it,mes,anio)

        }
    }

}
