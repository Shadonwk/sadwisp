package sadwisp

import grails.converters.JSON
import grails.util.Environment
import groovy.sql.Sql
import mx.com.wisnet.sadwisp.Configuracion
import mx.com.wisnet.sadwisp.Historial
import mx.com.wisnet.sadwisp.Mikrotik
import mx.com.wisnet.sadwisp.Plan
import mx.com.wisnet.sadwisp.seguridad.OLTHelperService
import org.grails.web.converters.configuration.DefaultConverterConfiguration

class BootStrap {

    def dataSource
    def grailsApplication
    def oltHelperService


    def init = { servletContext ->
        //if (Environment.current == Environment.DEVELOPMENT) {
        TimeZone.setDefault(TimeZone.getTimeZone("America/Mexico_City"))
        def baseDir = System.getProperty( 'catalina.base' )
        def imagesDir = "/tmp/assets/"
        println ("el primero " + imagesDir)
        //println("inicia conexion ssh test")
        //OLTHelperService test = new OLTHelperService()
        //println("termina conexion sshl")
        //test.connectOLT()
        if (!Configuracion.count()) {
            //loadSqlData('catalogos')
            loadSqlData('datos_prueba')
        }
        //}


        JSON.createNamedConfig('planes') { DefaultConverterConfiguration<JSON> cfg ->
            cfg.registerObjectMarshaller(Plan) { Plan it, JSON json ->
                def output = [:]
                output['id'] = it.id
                output['nombre'] = it.nombre
                return output
            }
        }

        JSON.createNamedConfig('mikrotiks') { DefaultConverterConfiguration<JSON> cfg ->
            cfg.registerObjectMarshaller(Mikrotik) { Mikrotik it, JSON json ->
                def output = [:]
                output['id'] = it.id
                output['nombre'] = it.nombre
                return output
            }
        }

        JSON.createNamedConfig('historial') { DefaultConverterConfiguration<JSON> cfg ->
            cfg.registerObjectMarshaller(Historial) { Historial it, JSON json ->
                def output = [:]
                output['id'] = it.id
                output['cliente'] = it.cliente.id
                output['mes'] = it.mes
                output['anio'] = it.anio
                output['statusPago'] = it.statusPago
                output['cantidad'] = it.cantidad
                return output
            }
        }

    }

    /**Se encarga de realizar la carga de un archivo sql a la base de datos**/
    def loadSqlData(archivo) {
        println "entra a cargar los datos"
        try {
            def db = new Sql(dataSource)
            def is = grailsApplication.mainContext.getResource("classpath:sql/${archivo}.sql").inputStream
            log.info "::::::::: Carga ${archivo}... 0% :::::::::"
            println "::::::::: Carga ${archivo}... 0% :::::::::"
            is.eachLine { line ->
                if(line.contains('set') && Environment.current == Environment.DEVELOPMENT){
                    log.trace 'SET omitido'
                } else if (line.trim().empty) {
                    log.trace 'línea en blanco omitida'
                } else {
                    log.info line
                    println line
                    db.executeUpdate(line)
                }
            }
            log.info "::::::::: Carga ${archivo}... 100% :::::::::"
        } catch (ex) {
            log.warn ex
            println ex


        }
    }


    def destroy = {
    }

}
