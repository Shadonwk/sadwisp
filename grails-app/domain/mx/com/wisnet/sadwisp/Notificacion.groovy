package mx.com.wisnet.sadwisp

class Notificacion {

    String mensaje
    //Usuario remitente
    Cliente destinatario
    Date fechaNotificacion
    Date fechaLectura
    Date dateCreated

    static constraints = {
        mensaje(blank: false, maxSize: 2000)
        //remitente()
        destinatario()
        fechaNotificacion(nullable: true)
        fechaLectura(nullable: true)
        dateCreated(nullable: true)
    }

    def notificada() {
        fechaNotificacion != null
    }

    def leida() {
        fechaLectura != null
    }

    String toString () {
        mensaje
    }

    /*
     * Regresa la lista de Notificaciones para un Usuario.
     *
     * @param params Mapa con los par�metros para el paginado.
     * @param usuario Usuario del que se requiere consultar las notificaciones.
     *
     * @return list con las notificaciones encontradas.
     */
    def static listaNotificacionesPorUsuario(params, usuario) {
        Notificacion.createCriteria().list(max:params.max, offset:params.offset, sort:params.sort, order:params.order){
            eq('destinatario', usuario)
            isNull('fechaLectura')

        }
    }

    def puedeSerAccedidaPor(usuario) {
        this.destinatario.id == usuario.id
    }
}
