package mx.com.wisnet.sadwisp

import me.legrange.mikrotik.ApiConnection

import javax.net.SocketFactory

class Mikrotik {

    //Usuario cuenta
    String ipMikrotik
    String usuarioMikrotik
    String passwordMikrotik
    int puerto
    String nombre
    static  hasMany = [planes:Plan]

    static constraints = {
        ipMikrotik nullable: false

    }

    String toString(){
        "IP: " + ipMikrotik
    }

    def clientes(){
        println "entrando a contar los clientes"
        Cliente.findAllByMikrotik(this).size()
    }

    def status(){
        try{
            println "Verificando conexion mikrotik"
            ApiConnection con =ApiConnection.connect( SocketFactory.getDefault(), ipMikrotik, puerto, 60000); // connect to router
            con.login(usuarioMikrotik,passwordMikrotik); // log in to router

            //ApiConnection con = ApiConnection.connect(ipMikrotik); // connect to router
            //con.login(usuarioMikrotik,passwordMikrotik); // log in to router
            return "Online"
        }catch (Exception e){
            return "Offline"
        }


    }
}
