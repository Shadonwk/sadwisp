package mx.com.wisnet.sadwisp


class Plan {

    String nombre
    String download
    String upload
    String uploadMedida
    String downloadMedida
    double costo
    String pcqDownload
    String pcqUpload
    String tipo
    static  belongsTo = [mikrotik:Mikrotik]
    String burstLimit
    String burstThreshold
    String burstTime
    String parent
    String localAddress
    String remoteAddress
    String tipoControlPPPOE //pude ser PCQ or SimpleQueues
    String addressList //la lista para el marcado de paquetes


    static mapping = {
        table 'tarifa'
    }


    static constraints = {
        pcqDownload nullable: true
        pcqUpload nullable: true
        download nullable: true
        upload nullable: true
        uploadMedida nullable: true
        downloadMedida nullable: true
        burstLimit nullable: true
        burstThreshold nullable: true
        burstTime nullable: true
        parent nullable: true
        localAddress nullable: true
        remoteAddress nullable: true
        tipoControlPPPOE nullable: true
        addressList nullable: true
    }


    String toString(){
        if(tipo.equals('PCQ')){
            return "$nombre ($pcqUpload / $pcqDownload)"

        }else if (tipo.equals('PPPOE PCQ')){
            return "$nombre ($addressList)"
        }else{
            return "$nombre ($download $downloadMedida / $upload $uploadMedida)"
        }

    }


}
