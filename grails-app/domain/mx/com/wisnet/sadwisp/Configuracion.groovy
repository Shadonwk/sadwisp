package mx.com.wisnet.sadwisp

class Configuracion {

    Integer consecutivoNotas
    String direccionFecha
    int diasGraciaCorte
    int diasRecordatorio
    String mensajeCorte
    String mensajeRecordatorio
    String idDispositivo
    String keyDispositivo
    boolean editarNota
    boolean postPago
    String telefono
    String codigoPostal
    String direccion
    String permisoIft
    String aviso
    String nombreLogo = "FUSION.png"
    String qrCode
    String barCode
    boolean secretManual
    boolean secretUpdate
    String nombreEmpresa


    static constraints = {
        idDispositivo nullable: true
        keyDispositivo nullable: true
        direccion nullable: true
        permisoIft nullable: true
        telefono nullable: true
        codigoPostal nullable: true
        aviso nullable: true
        nombreLogo nullable: true
        secretManual nullable:true
        secretUpdate nullable:true
        qrCode nullable: true
        barCode nullable: true
        nombreEmpresa nullable: true

    }

    static mapping = {
        aviso type: 'text'
    }
}
