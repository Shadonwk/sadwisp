package mx.com.wisnet.sadwisp.seguridad

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class Usuario implements Serializable {

    private static final long serialVersimonUID = 1

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    String nombre
    String apellidoPaterno
    String apellidoMaterno
    String email
    Date fechaAlta


    Set<Rol> getAuthorities() {
        (UsuarioRol.findAllByUsuario(this) as List<UsuarioRol>)*.rol as Set<Rol>
    }

    static constraints = {
        username(blank: false, unique: true, size: 5..20, matches: "[0-9A-Za-z_]+")
        password(blank: false, password: true)
        enabled()
        accountExpired()
        accountLocked()
        passwordExpired()
        nombre(blank: false, size: 2..100)
        apellidoPaterno(blank: false, size: 2..50)
        apellidoMaterno(nullable: true, size: 2..50)
        email(email: true, blank: false)
        fechaAlta()

    }

    static mapping = {
        password column: '`password`'
    }

    String getNombreCompleto() {
        [nombre, apellidoPaterno, apellidoMaterno ?: ''].collect {
            it.capitalize()
        }.join(' ').replaceAll("\\s+", " ").trim()
    }

    @Override
    String toString() {
        return getNombreCompleto()
    }

    Rol getRole() {
        UsuarioRol.findAllByUsuario(this).collect { it.rol }.first()
    }

    List<Rol> getRoles() {
        UsuarioRol.findAllByUsuario(this).collect { it.rol }
    }

    List<Rol> getOtherRoles() {
        def roles = Rol.list([sort:'nombre'])
        def owned = getRoles()
        roles.removeAll(owned)
        roles
    }

    boolean isOperativa() {
        enabled && !accountLocked && !accountExpired && !passwordExpired
    }

}
