package mx.com.wisnet.sadwisp.seguridad

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='nombre')
@ToString(includes='nombre', includeNames=true, includePackage=false)
class Rol implements Serializable {

	private static final long serialVersionUID = 1

	String nombre

	static constraints = {
		nombre nullable: false, blank: false, unique: true
	}

	static mapping = {
		cache true
	}

	/*
    * Regresa la lista de Roles.
    *
    * @param params Mapa con los parámetros para el paginado.
    *
    * @return list con los roles encontrados.
    */
	static lista(params) {
		Rol.list(max:params.max, offset:params.offset, sort:params.sort, order:params.order)
	}

	String toString(){
		nombre
	}
}
