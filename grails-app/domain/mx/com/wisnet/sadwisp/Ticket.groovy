package mx.com.wisnet.sadwisp

import mx.com.wisnet.sadwisp.seguridad.Usuario

class Ticket {

    String asunto
    String descripcion
    Cliente cliente
    String prioridad
    String status
    Double costo
    Double costoFinal
    String descripcionCierre

    static hasMany = [incidencias:Incidencia, tecnicos:Usuario]

    //propiedades de control
    Date dateCreated
    Date lastUpdated

    Usuario userCreated
    Usuario userUpdated


    static constraints = {
        costo nullable: true
        descripcionCierre nullable: true
        costoFinal nullable: true
    }
}
