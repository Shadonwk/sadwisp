package mx.com.wisnet.sadwisp

import mx.com.wisnet.sadwisp.seguridad.Usuario

class Incidencia {

    String descripcion

    static belongsTo = [ticket:Ticket]

    //propiedades de control
    Date dateCreated
    Date lastUpdated

    Usuario userCreated
    Usuario userUpdated

    static constraints = {
    }
}
