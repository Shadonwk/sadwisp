package mx.com.wisnet.sadwisp

import mx.com.wisnet.sadwisp.seguridad.Usuario

import java.time.LocalDate
import java.time.Month
import java.time.Period

class Historial implements Comparable{

    static transactional = true

    Cliente cliente
    Integer mes
    Integer anio
    Boolean statusPago
    LocalDate fechaPago
    Usuario usuarioCobro
    Double cantidad


    def getMesLetra() {
        def result

        switch(mes) {
            case [1]:
                result = 'Enero'
                break
            case [2]:
                result = 'Febrero'
                break
            case [3]:
                result = 'Marzo'
                break
            case [4]:
                result = 'Abril'
                break
            case [5]:
                result = 'Mayo'
                break
            case [6]:
                result = 'Junio'
                break
            case [7]:
                result = 'Julio'
                break
            case [8]:
                result = 'Agosto'
                break
            case [9]:
                result = 'Septiembre'
                break
            case [10]:
                result = 'Octubre'
                break
            case [11]:
                result = 'Noviembre'
                break
            case [12]:
                result = 'Diciembre'
                break
            default:
                result = 'No especificado'
        }

        result
    }

    static constraints = {
        fechaPago nullable: true
        usuarioCobro nullable: true
        cantidad nullable: true
    }


    @Override
    int compareTo(Object o) {
        return this.id-o.id
    }


}
