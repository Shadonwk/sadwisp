package mx.com.wisnet.sadwisp

import org.apache.tomcat.jni.Local

import java.time.LocalDate

class Cliente {

    static transactional = true

    //Usuario cuenta
    String nombre
    String apellidoPaterno
    String apellidoMaterno
    String email
    String telefono
    String ip
    String direccion
    String macAntena
    LocalDate fechaAlta
    int diaCorte
    boolean renta
    String comentarios

    String name
    String statusServicio = "EN_LINEA"
    Mikrotik mikrotik
    Plan plan
    String caja

    String latitud
    String longitud


    static constraints = {
        telefono(nullable:true)
        nombre(nullable: true, blank: false, size: 2..100)
        apellidoPaterno(nullable: true,blank: false, size: 2..50)
        apellidoMaterno(nullable: true,blank: false, size: 2..50)
        email(nullable: true,email: true, blank: false)
        fechaAlta(nullable: true)
        diaCorte range: 1..30
        ip nullable: true
        macAntena  nullable: true, matches: "[0-9a-fA-F][0-9a-fA-F][:-][0-9a-fA-F][0-9a-fA-F][:-][0-9a-fA-F][0-9a-fA-F][:-][0-9a-fA-F][0" +
                "-9a-fA-F][:-][0-9a-fA-F][0-9a-fA-F][:-][0-9a-fA-F][0-9a-fA-F]"
        name nullable: true, unique: true
        comentarios nullable: true
        direccion nullable: true
        caja nullable: true
        latitud nullable: true
        longitud nullable: true
    }

    /*
     * Regresa la lista de Usuarios.
     * @param params Mapa con los parámetros para el paginado.
     * @return list con los usuarios encontrados.
     */
    def static lista(params) {
        Cliente.list(max:params.max, offset:params.offset, sort:params.sort, order:params.order)
    }

    String getNombreCompleto() {
        nombre?.capitalize() + ' ' + apellidoPaterno?.capitalize() + ' ' +(apellidoMaterno?apellidoMaterno?.capitalize():"")
    }

    String toString(){
        getNombreCompleto()
    }

    String getEstatusMesActual(){
        LocalDate dt = LocalDate.now();
        Integer month = dt.getMonthValue();  // where January is 1 and December is 12
        int year = dt.getYear();

        def historialActual =Historial.findByClienteAndMesAndAnio(this,month,year)

        if(historialActual.statusPago){
            return "Pagado"
        } else{
            return "Pendiente"
        }


    }

}
