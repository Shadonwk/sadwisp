package mx.com.wisnet.sadwisp.seguridad

import com.jcraft.jsch.Channel
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import grails.gorm.transactions.Transactional

@Transactional
class OLTHelperService {

    def connectOLT() {

        Session session

        session = new JSch().getSession("Shadonwk", "96b10a64adeb.sn.mynetname.net", 22);
        session.setPassword("joseleon22");
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        Channel channel=session.openChannel("exec");
        ((ChannelExec)channel).setCommand("pwd");
        channel.setInputStream(null);
        ((ChannelExec)channel).setErrStream(System.err);

        InputStream input = channel.getInputStream();
        channel.connect();
        byte[] tmp=new byte[1024];
        while(true){
            while(input.available()>0){
                int i=input.read(tmp, 0, 1024);
                if(i<0)break;
                System.out.print(new String(tmp, 0, i));
            }
            if(channel.isClosed()){
                System.out.println("exit-status: "+channel.getExitStatus());
                break;
            }
            try{Thread.sleep(1000);}catch(Exception ee){}
        }
        channel.disconnect();
        session.disconnect();
        System.out.println("DONE");
    }
}
