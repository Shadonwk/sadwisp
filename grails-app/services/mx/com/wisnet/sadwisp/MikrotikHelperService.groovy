package mx.com.wisnet.sadwisp

import grails.gorm.transactions.Transactional
import me.legrange.mikrotik.ApiConnection

import javax.net.SocketFactory
import java.time.LocalDate
import java.time.Month
import java.time.Period

@Transactional
class MikrotikHelperService {

    ApiConnection con
    def clienteService
    def historialService


    def conexion(ipMikrotik, user, password, port){
        //con = ApiConnection.connect(ipMikrotik); // connect to router
        con = ApiConnection.connect( SocketFactory.getDefault(), ipMikrotik, port, 60000); // connect to router
        con.login(user,password); // log in to router
        log.debug "se realizo la conexion.."
        return con
    }

    def detalles(Mikrotik mikrotik){
        List<Map<String, String>> rs
        List detallesList = new ArrayList()

        try {

            con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
            rs= con.execute('/system/resource/print')

            for (Map<String,String> r : rs) {

                detallesList.add(r.get("board-name"))
                detallesList.add(r.get("cpu"))
                detallesList.add(r.get("version"))
            }

        }catch (Exception e){
            println "Error en la conexión intente de nuevo."
        }
        return  detallesList
    }

    def getPCQ(Mikrotik mikrotik){

        List<Map<String, String>> rs
        List pcqList = new ArrayList()
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        try {
            rs= con.execute('/queue/type/print')
            for (Map<String,String> r : rs) {
                println(r.get("name"));
                pcqList.add(r.get("name"))
            }
        }catch (Exception e){

        }
        return  pcqList
    }

    def getIpPools(Mikrotik mikrotik){

        List<Map<String, String>> rs
        List poolList = new ArrayList()
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        try {
            rs= con.execute('/ip/pool/print')
            for (Map<String,String> r : rs) {
                println(r.get("name"));
                poolList.add(r.get("name"))
            }
        }catch (Exception e){

        }
        return  poolList
    }

    def getProfilesHotspot(Mikrotik mikrotik){
        List<Map<String, String>> rs
        List profileList = new ArrayList()
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        try {
            rs= con.execute('/ip/hotspot/user/profile/print')
            for (Map<String,String> r : rs) {
                println(r.get("name"));
                profileList.add(r.get("name"))
            }
        }catch (Exception e){

        }
        return  profileList
    }

    def reactivarServicio(Cliente cliente)
    {
        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        try {

            if(cliente.plan.tipo.equals("PPPOE")||cliente.plan.tipo.equals('PPPOE PCQ')){
                log.debug " reactivando el servicio "+ cliente.nombreCompleto
                con.execute("/ppp/secret/enable numbers='$cliente.name'")

            }else if (cliente.plan.tipo.equals('Simple Queue') || (cliente.plan.tipo.equals('PCQ'))){
                log.debug " reactivando el servicio "+ cliente.ip
                def item = con.execute("/ip/firewall/address-list/print where address=$cliente.ip")
                log.debug "item: "+ item
                def idEliminar = item.get(0).get(".id")
                log.debug "id a eliminar" + idEliminar
                con.execute("/ip/firewall/address-list/remove .id=$idEliminar")

            }else{
                //hotspot
                log.debug " reactivando el servicio "+ cliente.nombreCompleto
                con.execute("/ip/hotspot/user/enable numbers='$cliente.macAntena'")

                //buscar conexión activa en host y eliminar
                //Buscar la conexion activa
                def item = con.execute("/ip/hotspot/host/print where mac-address='$cliente.macAntena'")
                println item
                //Si tiene una conexion activa eliminarla....
                if(!item.isEmpty()){
                    def idEliminar = item.get(0).get(".id")
                    println "id a eliminar" + idEliminar
                    //Borrar la conexion activa
                    con.execute("/ip/hotspot/host/remove .id='$idEliminar'")
                }

            }

            return 1

        }catch (Exception e){
            println "error al reactivar el servicio"
            e.printStackTrace()
            return 0
        }
    }


    def suspenderServicio(Cliente cliente){

        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)

        try {
            if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
                //Buscar el secret e inhabilitarlo

                con.execute("/ppp/secret/disable numbers='$cliente.name'")
                //Buscar la conexion activa
                def item = con.execute("/ppp/active/print where name='$cliente.name'")
                println item
                //Si tiene una conexion activa eliminarla....
                if(!item.isEmpty()){
                    def idEliminar = item.get(0).get(".id")
                    println "id a eliminar" + idEliminar
                    //Borrar la conexion activa
                    con.execute("/ppp/active/remove .id='$idEliminar'")
                }


            }else if(cliente.plan.tipo.equals('Simple Queue') || (cliente.plan.tipo.equals('PCQ'))){
                def rs1 = con.execute("/ip/firewall/address-list/add address=" + cliente.ip + " list=deudores");
                println rs1
            }
            else{
                //para hotspo
                con.execute("/ip/hotspot/user/disable numbers='$cliente.macAntena'")

                //Buscar la conexion activa
                def item = con.execute("/ip/hotspot/active/print where user='$cliente.macAntena'")
                println item
                //Si tiene una conexion activa eliminarla....
                if(!item.isEmpty()){
                    def idEliminar = item.get(0).get(".id")
                    println "id a eliminar" + idEliminar
                    //Borrar la conexion activa
                    con.execute("/ip/hotspot/active/remove .id='$idEliminar'")
                }

            }
            }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def sendPlanPPPOEPCQ(Plan plan){
        def mikrotik = plan.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)

        try {
            con.execute("/ppp/profile/add name='$plan.nombre' local-address='$plan.localAddress'" +
                        " remote-address='$plan.remoteAddress' only-one='yes' " +
                        "address-list='$plan.addressList'")

        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def sendPlanPPPOE(Plan plan){
        def mikrotik = plan.mikrotik


        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        // ppp profile add name=test remote-address=POOL_PPPOE rate-limi
        //t=100M/10M
        try {
            con.execute("/ppp/profile/add name='$plan.nombre' local-address='$plan.localAddress'" +
                    " remote-address='$plan.remoteAddress' only-one='yes' " +
                    "rate-limit=$plan.upload$plan.uploadMedida/$plan.download$plan.downloadMedida")
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def sendPCQ(Cliente cliente){
        def mikrotik = cliente.mikrotik
        //def plan = cliente.plan
        //con = ApiConnection.connect(conexion.ipMikrotik); // connect to router
        ////con = ApiConnection.connect(ssl, conexion.ipMikrotik, ApiConnection.DEFAULT_TLS_PORT, ApiConnection.DEFAULT_CONNECTION_TIMEOUT);
        //con.login(conexion.usuarioMikrotik,conexion.passwordMikrotik); // log in to router
        //println "se realizo la conexion.."

        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        try {
            con.execute("/queue/simple/add name='$cliente.name' target=$cliente.ip queue=$cliente.plan.pcqUpload/$cliente.plan.pcqDownload")
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def deleteClienteHostpot(Cliente cliente){
        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)

        try {
            con.execute("/ip/hotspot/user/remove numbers='$cliente.macAntena'")
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def deleteQueue(Cliente cliente){
        def mikrotik = cliente.mikrotik
        def plan = cliente.plan
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)

        try {
            //con = ApiConnection.connect(conexion.ipMikrotik);
            ////    con = ApiConnection.connect(ssl, conexion.ipMikrotik, ApiConnection.DEFAULT_TLS_PORT, ApiConnection.DEFAULT_CONNECTION_TIMEOUT);
            //con.login(conexion.usuarioMikrotik,conexion.passwordMikrotik); // log in to router
            con.execute("/queue/simple/remove .id=$cliente.name")
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }


    def deleteClientePPPOE(Cliente cliente){
        def mikrotik = cliente.mikrotik

        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)

        try {
            //con = ApiConnection.connect(conexion.ipMikrotik);
            ////    con = ApiConnection.connect(ssl, conexion.ipMikrotik, ApiConnection.DEFAULT_TLS_PORT, ApiConnection.DEFAULT_CONNECTION_TIMEOUT);
            //con.login(conexion.usuarioMikrotik,conexion.passwordMikrotik); // log in to router
            con.execute("/ppp/secret/remove numbers=$cliente.name")
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }
    def sendClientePPPOE(Cliente cliente){
        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        //ppp secret add name="Roberto2" password=test profile=20MEGAS service=pppoe comment=desdeShell

        try {
            con.execute("/ppp/secret/add name='$cliente.name' password='$cliente.name' service='pppoe' profile='$cliente.plan.nombre'")

        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true


    }

    def updateClientePPPOE(Cliente cliente){
        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        //ppp secret add name="Roberto2" password=test profile=20MEGAS service=pppoe comment=desdeShell

        try {
            con.execute("/ppp/secret/set numbers='$cliente.name' profile='$cliente.plan.nombre'")

        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true


    }


    def sendQueue(Cliente cliente){
        def mikrotik = cliente.mikrotik
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        def plan = cliente.plan

        def upload
        def download
        def burstLimit
        def burstThreshold
        def burstTime = plan.burstTime +"s"
        def parent = plan.parent
        if("KB".equals(plan.uploadMedida)){
            upload = plan.upload+"K"
            burstLimit = plan.burstLimit+"K"
            burstThreshold = plan.burstThreshold + "K"
        }else{
            upload = plan.upload+"M"
            burstLimit = plan.burstLimit+"M"
            burstThreshold = plan.burstThreshold + "M"
        }

        if("KB".equals(plan.downloadMedida)){
            download = plan.download+"K"
        }else{
            download = plan.download+"M"
        }

        println "velocidades $upload $download rafaga $burstLimit $burstThreshold $burstTime"

        try {

            if(null == parent){
                con.execute("/queue/simple/add name='$cliente.name' target=$cliente.ip max-limit=$upload/$download burst-threshold=$burstThreshold/$burstThreshold burst-limit=$burstLimit/$burstLimit burst-time=$burstTime/$burstTime")
            }else{
                con.execute("/queue/simple/add name='$cliente.name' target=$cliente.ip max-limit=$upload/$download burst-threshold=$burstThreshold/$burstThreshold burst-limit=$burstLimit/$burstLimit burst-time=$burstTime/$burstTime parent=$parent")
            }
        }catch (Exception e){
            println "errores;"
            println e.message
            con.close()
            return false
        }
        con.close()
        return true
    }

    def importarClientesPPPOE(Mikrotik mikrotik){
        List<Map<String, String>> rs
        List pcqList = new ArrayList()
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        HistorialController historialController = new HistorialController()

        try {
            rs= con.execute('/ppp/secret/print')
            for (Map<String,String> r : rs) {
                println("**********************")
                println(r.get("name"));
                println(r.get("password"));
                println(r.get("profile"));
                def plan = Plan.findByNombre(r.get("profile"))
                println ("El plan es : " + plan.id)
                def cliente = new Cliente()
                cliente.nombre = r.get("name")
                cliente.name = r.get("name")
                cliente.apellidoPaterno = "Ajustar"
                cliente.apellidoMaterno = "Ajustar"
                cliente.caja = "Ajustar"
                cliente.direccion = "Ajustar"
                cliente.mikrotik = mikrotik
                cliente.plan = plan
                cliente.diaCorte = 15
                println("validate:" + cliente.validate())
                println("**********************")
                clienteService.save(cliente)
                generateHistorial(cliente)

            }

        }catch (Exception e){
            return false
        }
        return  true

    }


    def importarClientes(Mikrotik mikrotik) {

        List<Map<String, String>> rs
        List pcqList = new ArrayList()
        con = conexion(mikrotik.ipMikrotik, mikrotik.usuarioMikrotik, mikrotik.passwordMikrotik, mikrotik.puerto)
        HistorialController historialController = new HistorialController()

        try {
            rs= con.execute('/queue/simple/print')
            for (Map<String,String> r : rs) {
                println(r.get("name"));
                println(r.get("target"));
                println(r.get("queue"))
                def cliente = new Cliente()
                cliente.nombre = r.get("name")
                cliente.name = r.get("name")
                cliente.mikrotik = mikrotik
                cliente.ip = r.get("target").split("/")[0]
                cliente.diaCorte = 1
                if(r.get("queue").equals("default-small/default-small")){
                    //significa que es simple
                    def velocidad = r.get("max-limit").split("/")
                    def plan = Plan.findByDownloadAndUpload(velocidad[1].toInteger()/1000000,velocidad[0].toInteger()/1000000)
                    println(plan)
                    if (null!=plan){
                        cliente.plan = plan
                    }else{
                        return false
                    }
                }else{
                    //significa que es pcq
                    def velocidad = r.get("queue").split("/")
                    def plan = Plan.findByPcqUploadAndPcqDownload(velocidad[0],velocidad[1])
                    println(plan)
                    if (null!=plan){
                        cliente.plan = plan
                    }else{
                        return false
                    }


                }
                clienteService.save(cliente)
                generateHistorial(cliente)

            }

        }catch (Exception e){
            return false
        }
        return  true

    }

    def generateHistorial(Cliente cliente) {

        LocalDate dt = LocalDate.now();
        String month = dt.month;  // where January is 1 and December is 12
        int year = dt.year;

        LocalDate lastDayOfYear = LocalDate.of(year,Month.DECEMBER,31)

        Period period = dt.until(lastDayOfYear);


        def months = period.getMonths() + 1
        months.times {
            def historial = new Historial()
            historial.setCliente(cliente)
            historial.setMes(dt.plusMonths(it).getMonthValue()) //todo guardar mes actual
            historial.setAnio(year) //todo guardar año acutal
            historial.setStatusPago(false) //para la primera vez siempre sera pendiente
            historialService.save(historial)
        }
    }

    def buscarRegistroClienteAndMesAndAnio(it,mes,anio){
        def historialActual = Historial.findByClienteAndMesAndAnio(it, mes, anio)
        if(historialActual==null){
            def nuevoHistorial = new Historial()
            nuevoHistorial.setAnio(anio)
            nuevoHistorial.setMes(mes)
            nuevoHistorial.setCliente(it)
            nuevoHistorial.setStatusPago(false)
            historialService.save(nuevoHistorial)
            println "se agrego el historial correctamente"
        }

    }
}
