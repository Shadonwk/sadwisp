package mx.com.wisnet.sadwisp

import grails.gorm.services.Service

@Service(Configuracion)
interface ConfiguracionService {

    Configuracion get(Serializable id)

    List<Configuracion> list(Map args)

    Long count()

    void delete(Serializable id)

    Configuracion save(Configuracion configuracion)

}