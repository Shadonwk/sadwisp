package mx.com.wisnet.sadwisp

import grails.gorm.services.Service

@Service(Historial)
interface HistorialService {

    Historial get(Serializable id)

    List<Historial> list(Map args)

    Long count()

    void delete(Serializable id)

    Historial save(Historial historial)

}