package mx.com.wisnet.sadwisp

import grails.gorm.services.Service

@Service(Mikrotik)
interface MikrotikService {

    Mikrotik get(Serializable id)

    List<Mikrotik> list(Map args)

    Long count()

    void delete(Serializable id)

    Mikrotik save(Mikrotik mikrotik)

}