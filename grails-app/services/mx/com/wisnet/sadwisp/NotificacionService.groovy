package mx.com.wisnet.sadwisp

import grails.gorm.services.Service

@Service(Notificacion)
interface NotificacionService {

    Notificacion get(Serializable id)

    List<Notificacion> list(Map args)

    Long count()

    void delete(Serializable id)

    Notificacion save(Notificacion notificacion)

}