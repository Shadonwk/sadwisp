package mx.com.wisnet.sadwisp

import mx.com.wisnet.sadwisp.seguridad.Usuario

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters

class EscritorioController {

    def index() {

        def clientesLinea = 0
        def clientesSuspendidos = 0
        def pagosHoy = 0
        def pagosMes = 0


        clientesLinea = Cliente.findAllByStatusServicio("EN_LINEA").size()
        clientesSuspendidos = Cliente.findAllByStatusServicio("SUSPENDIDO").size()

        def pagos = Historial.findAllByFechaPago(LocalDate.now())
        pagos.each {
            def cliente = it.cliente
            pagosHoy += cliente.plan.costo
        }

        def fecha = LocalDate.now()
        def ultimoDiaMes = fecha.with(TemporalAdjusters.lastDayOfMonth())
        def primerDiaMes = fecha.with(TemporalAdjusters.firstDayOfMonth())
        def pagosM = Historial.findAllByFechaPagoBetween(primerDiaMes, ultimoDiaMes)

        pagosM.each {
            pagosMes+= it.cliente.plan.costo
        }

        def usuarios = Usuario.findAllByEnabled(true)
        def resumen = [:]

        usuarios.each {
            def totalCobros = 0
            def registros  = Historial.findAllByFechaPagoBetweenAndUsuarioCobro(primerDiaMes, ultimoDiaMes, it)
            if(!registros.isEmpty())
               registros.each {
                   totalCobros += it.cliente.plan.costo
               }
            resumen[it.nombreCompleto] = '$'+totalCobros
        }

        //calculo super básico traer todos los clientes y sumar sus paquetes

        double totalEnMes= Cliente.list().stream()
                .mapToDouble({ cliente -> cliente.plan.costo })
                .sum();

        def pagadoEnMes = pagosMes
        def porCobrarEnMes = totalEnMes-pagadoEnMes

        def clientesNuevos = ['semana1':10, 'semana2':5, 'semana3':3, 'semana4':4]

        [clientesLinea:clientesLinea, clientesSuspendidos:clientesSuspendidos, pagosHoy:pagosHoy,
            pagosMes:pagosMes, resumen: resumen, pagadoEnMes:pagadoEnMes, porCobrarEnMes:porCobrarEnMes, clientesNuevos:clientesNuevos]

    }


    def mostrarDiagrama(){

    }
}
