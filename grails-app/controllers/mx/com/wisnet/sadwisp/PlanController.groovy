package mx.com.wisnet.sadwisp

import grails.converters.JSON
import grails.validation.ValidationException
import mx.com.wisnet.sadwisp.dto.MessageSMSDTO
import mx.com.wisnet.sadwisp.util.Conexion
import org.grails.web.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

import java.security.MessageDigest

import static org.springframework.http.HttpStatus.*

class PlanController {

    PlanService planService
    MikrotikHelperService mikrotikHelperService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        respond planService.list(params), model:[planCount: planService.count()]
    }

    def show(Long id) {
        respond planService.get(id)
    }

    def create() {
        def total = Plan.count()
        //println getSerialNumber("c")
        //if(total<10){
            respond new Plan(params)
        //}else{
        //    flash.message = message(code: 'lisencia.max.created.message')
        //    redirect(action: 'index')
        //}

    }

    def save(Plan plan) {
        if (plan == null) {
            notFound()
            return
        }

        try {
            planService.save(plan)
        } catch (ValidationException e) {
            respond plan.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'plan.label', default: 'Plan'), plan.id])
                redirect plan
            }
            '*' { respond plan, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond planService.get(id)
    }

    def update(Plan plan) {
        if (plan == null) {
            notFound()
            return
        }

        try {
            planService.save(plan)
        } catch (ValidationException e) {
            respond plan.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'plan.label', default: 'Plan'), plan.id])
                redirect plan
            }
            '*'{ respond plan, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        planService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'plan.label', default: 'Plan'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'plan.label', default: 'Plan'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def renderPCQ(){
        println params
        def mikrotik = Mikrotik.get(params.mikrotik)
        if(params.tipo.equals('PPPOE')){
            def poolList = mikrotikHelperService.getIpPools(mikrotik)
            render template: 'PPPOE', model: [poolList:poolList]
            return
        }
        else if(params.tipo.equals("PCQ")){
            def pcqList = mikrotikHelperService.getPCQ(mikrotik)
            render template: 'pcq', model: [uploadList:pcqList, downloadList:pcqList]
            return
        }else if(params.tipo.equals('PPPOE PCQ')){
            def poolList = mikrotikHelperService.getIpPools(mikrotik)
            render template: 'PPPOEPCQ', model: [poolList:poolList]
            return
        }else if (params.tipo.equals('Hotspot')){
            def profiles = mikrotikHelperService.getProfilesHotspot(mikrotik)
            render template: 'hotspot', model: [profileList:profiles]
            return
        }
        else{
            render template: 'simpleQueue', model: []
        }

    }

    def sendMikrotik(Long id){
        def plan = planService.get(id)
        log.info "Enviando plan al mk" + plan.nombre

        def creado = false
        if(plan.tipo.equals('PPPOE')){
            creado = mikrotikHelperService.sendPlanPPPOE(plan)
        }else if(plan.tipo.equals("PPPOE PCQ")){
            creado = mikrotikHelperService.sendPlanPPPOEPCQ(plan)
        }else{
            //creado = mikrotikHelperService.sendQueue(cliente)
        }

        if (creado){
            flash.message="Se realizo la creación del plan pppoe correctamente"
            redirect(action:"show", id: plan.id)
        }else{
            flash.errorMessage = "Ha ocurrido un error intente más tarde"
            redirect(action:"show", id: plan.id)
            return
        }


    }

    public static String getSerialNumber(String drive) {
        String result = "";
        try {

            Process p = Runtime.getRuntime().exec("wmic diskdrive get serialnumber");
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return result.trim();
    }

}
