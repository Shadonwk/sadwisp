package mx.com.wisnet.sadwisp

import grails.validation.ValidationException
import me.legrange.mikrotik.ApiConnection

import java.time.LocalDate
import java.time.Month
import java.time.Period

import static org.springframework.http.HttpStatus.*

class ClienteController {

    ClienteService clienteService
    MikrotikHelperService mikrotikHelperService
    HistorialService historialService
    def historialController
    def mailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = 'diaCorte'
        respond clienteService.list(params), model:[clienteCount: clienteService.count()]
    }

    def clientesBloqueados(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = 'diaCorte'
        def clientes =Cliente.findAllByStatusServicio("SUSPENDIDO")
        respond clientes, model:[clienteCount: clientes.size()]
    }

    def show(Long id) {
        respond clienteService.get(id)
    }

    def create() {
        def total = Cliente.count()
        //if(total<100){
            respond new Cliente(params)
        //}else{
        //    flash.message = message(code: 'lisencia.max.created.message')
        //    redirect(action: 'index')
        //}

    }

    def createPPPOE() {
        def total = Cliente.count()
        //if(total<100){
        respond new Cliente(params)
        //}else{
        //    flash.message = message(code: 'lisencia.max.created.message')
        //    redirect(action: 'index')
        //}

    }

    def createHotspot() {

        mailService.sendMail {
            to "paulino.12500@gmail.com","j.roberto.leon@gmail.com"
            from "test@treisatransportes.com"
            subject "Hola Perros desde Sadwiso"
            text 'Este es un mensaje generado automaticamente'
        }


        def total = Cliente.count()
        //if(total<100){
        respond new Cliente(params)
        //}else{
        //    flash.message = message(code: 'lisencia.max.created.message')
        //    redirect(action: 'index')
        //}

    }

    def save(Cliente cliente) {
        if (cliente == null) {
            notFound()
            return
        }

        if(cliente.plan.tipo.equals('Simple Queue')){
            cliente.name = cliente.nombre.replace(' ', '').toUpperCase()+"_"+cliente.apellidoPaterno.replace(' ','').toUpperCase()+"_"+cliente.apellidoMaterno.replace(' ', '').toUpperCase()
        }
        if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
            cliente.name = cliente.nombre.replace(' ', '').toUpperCase()+cliente.apellidoPaterno.replace(' ','').toUpperCase()+cliente.apellidoMaterno.replace(' ', '').toUpperCase()
        }
        if(cliente.plan.tipo.equals('Hotspot')){
            cliente.name = cliente.nombre.replace(' ', '').toUpperCase()+cliente.apellidoPaterno.replace(' ','').toUpperCase()+cliente.apellidoMaterno.replace(' ', '').toUpperCase()
        }

        try {
            clienteService.save(cliente)

            if(params.primerMes.equals('true')){
                //registrar el primer mes
                generateHistorial(cliente, true)
            }else{
                generateHistorial(cliente, false)
            }

        } catch (ValidationException e) {
            if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
                respond cliente.errors, view:'createPPPOE'
            }else if(cliente.plan.tipo.equals('Simple Queue')){
                respond cliente.errors, view:'create'
            }else{
                respond cliente.errors, view:'createHotspot'
            }
            return

        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cliente.label', default: 'Cliente'), cliente.id])
                redirect cliente
            }
            '*' { respond cliente, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond clienteService.get(id)
    }

    def sendMikrotik(Long id){
        def cliente = clienteService.get(id)
        print "Enviando cliente al mk" + cliente.nombre

        def creado = false
        if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
            creado = mikrotikHelperService.sendClientePPPOE(cliente)
        }else if(cliente.plan.tipo.equals("PCQ")){
            creado = mikrotikHelperService.sendPCQ(cliente)
        }else{
            creado = mikrotikHelperService.sendQueue(cliente)
        }




        if (creado){
            flash.message="Se realizo la creación del simple queue correctamente"
            redirect(action:"show", id: cliente.id)
        }else{
            flash.errorMessage = "Ha ocurrido un error intente más tarde"
            redirect(action:"show", id: cliente.id)
            return
        }


    }

    def update(Cliente cliente) {
        if (cliente == null) {
            notFound()
            return
        }

        def configuracion = Configuracion.get(1)

        if((cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')) && configuracion.secretUpdate){
            cliente.name = cliente.nombre.replace(' ', '').toUpperCase()+cliente.apellidoPaterno.replace(' ','').toUpperCase()+cliente.apellidoMaterno.replace(' ', '').toUpperCase()
        }

        try {
            clienteService.save(cliente)


            if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
                mikrotikHelperService.updateClientePPPOE(cliente)
            }else if(cliente.plan.tipo.equals("PCQ")){
                //actualizar plan pcq
                log.print("actualizar pcq")
            }

        } catch (ValidationException e) {
            respond cliente.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'cliente.label', default: 'Cliente'), cliente.id])
                redirect cliente
            }
            '*'{ respond cliente, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        def cliente = clienteService.get(id)

        def result = false
        if(cliente.plan.tipo.equals('PPPOE')||cliente.plan.tipo.equals('PPPOE PCQ')){
            result =mikrotikHelperService.deleteClientePPPOE(cliente)
        }else if(cliente.plan.tipo.equals("Simple Queue")){
            //borrar la queue
            result = mikrotikHelperService.deleteQueue(cliente)
        }else if (cliente.plan.tipo.equals("Hotspot")){
            result = mikrotikHelperService.deleteClienteHostpot(cliente)
        }


        if(result){
            def historial = Historial.findAllByCliente(cliente)
            historial.each {historialService.delete(it.id)}
            clienteService.delete(id)
        }else{
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'mikrotik.conexion.error')
                    redirect(action: 'show',id: cliente.id)
                }
                '*'{ render status: NO_CONTENT }
            }
            return
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'cliente.label', default: 'Cliente'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cliente.label', default: 'Cliente'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def generateHistorial(Cliente cliente, Boolean primerMes) {

        LocalDate dt = LocalDate.now();
        String month = dt.month;  // where January is 1 and December is 12
        int year = dt.year;

        LocalDate lastDayOfYear = LocalDate.of(year,Month.DECEMBER,31)

        Period period = dt.until(lastDayOfYear);


        def months = period.getMonths() + 1

        def banderaMesActual = 0
        if(primerMes){
            banderaMesActual = 1
            months = months -1
            def historial = new Historial()
            historial.setCliente(cliente)
            historial.setMes(dt.getMonthValue()) //todo guardar mes actual
            historial.setAnio(year) //todo guardar año acutal
            historial.setStatusPago(true) //para la primera vez siempre sera pendiente
            historial.setCantidad(0.0)
            historial.setFechaPago(LocalDate.now())
            historialService.save(historial)

        }

        months.times {
            it = it + banderaMesActual
            def historial = new Historial()
            historial.setCliente(cliente)
            historial.setMes(dt.plusMonths(it).getMonthValue()) //todo guardar mes actual
            historial.setAnio(year) //todo guardar año acutal
            historial.setStatusPago(false) //para la primera vez siempre sera pendiente
            historialService.save(historial)
        }
    }


    def buscar() {
        // Guarda bitácora


        params.max = params.max ?: 10
        params.offset = params.offset ?: 0
        params.sort = params.sort ?: 'name'
        params.order = params.order ?: 'asc'
        params.query = params.query?.trim() ?: ''

        // Si tiene un query de búsqueda lo usa para filtrar, de lo contrario muestra la lista de usuarios
        def lista = params.query.empty ? Cliente.lista(params) : busquedaEspecifica(params)
        def total = lista.totalCount

        render(view: 'index',  model:[clienteList: lista,clienteCount: total])
    }

    def busquedaEspecifica(params){
        def escapedQuery =  params.query.trim().replace('(', '\\(').replace(')', '\\)')
        def resultado = null
        resultado = Cliente.createCriteria().list(max:params.max, offset:params.offset, sort:params.sort, order:params.order){
            or {
                ilike("nombre", "%${escapedQuery}%")
                ilike("ip",   "%${escapedQuery}%")
                ilike("name",  "%${escapedQuery}%")
            }
        }

        if (resultado.isEmpty()){
            def busqueda = escapedQuery.split(" ")
            resultado = Cliente.createCriteria().list(max:params.max, offset:params.offset, sort:params.sort, order:params.order){
                and {
                    ilike("nombre", "%${busqueda[0]}%")
                    ilike("apellidoPaterno",   "%${busqueda[1]}%")

                }
            }
        }

        return resultado
    }

    def getListaClientes(){

        println "Se invoca la lista de cientes"

        def payload = "[\n" +
                "    {\n" +
                "        \"id\": \"7d6a58c3-50cd-4cfc-9e98-321b8f265cfd\",\n" +
                "        \"folio\": 3,\n" +
                "        \"descripcion\": \"CONGELADOS\",\n" +
                "        \"estatusInventario\": \"EN_CONTEO\",\n" +
                "        \"inventarista\": {\n" +
                "            \"id\": 11,\n" +
                "            \"nombre\": \"JOSE MANUEL\",\n" +
                "            \"aPaterno\": \"MORENO\",\n" +
                "            \"aMaterno\": \"REYES\"\n" +
                "        },\n" +
                "        \"tienda\": {\n" +
                "            \"id\": 1,\n" +
                "            \"descripcion\": \"T301 Apaseo Independencia\"\n" +
                "        }\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": \"98824703-5834-41ec-8003-de20b6bda0c3\",\n" +
                "        \"folio\": 2,\n" +
                "        \"descripcion\": \"BOTANAS Y CERVEZA\",\n" +
                "        \"estatusInventario\": \"PROCESO\",\n" +
                "        \"inventarista\": {\n" +
                "            \"id\": 9,\n" +
                "            \"nombre\": \"NANCY\",\n" +
                "            \"aPaterno\": \"RIVERA\",\n" +
                "            \"aMaterno\": \"HERNANDEZ\"\n" +
                "        },\n" +
                "        \"tienda\": {\n" +
                "            \"id\": 1,\n" +
                "            \"descripcion\": \"T301 Apaseo Independencia\"\n" +
                "        }\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": \"a8f7e2ee-4ba5-4adb-a00b-9dcea42252ce\",\n" +
                "        \"folio\": 1,\n" +
                "        \"descripcion\": \"ORDEN DE ABARROTES\",\n" +
                "        \"estatusInventario\": \"ABIERTO\",\n" +
                "        \"inventarista\": {\n" +
                "            \"id\": 7,\n" +
                "            \"nombre\": \"LUDWING\",\n" +
                "            \"aPaterno\": \"MARTINEZ\",\n" +
                "            \"aMaterno\": \"DE JESUS\"\n" +
                "        },\n" +
                "        \"tienda\": {\n" +
                "            \"id\": 1,\n" +
                "            \"descripcion\": \"T301 Apaseo Independencia\"\n" +
                "        }\n" +
                "    }\n" +
                "]"
        response.setContentType("application/json;charset=UTF-8")
        render payload
    }
}
