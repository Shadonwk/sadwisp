package mx.com.wisnet.sadwisp

import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import mx.com.wisnet.sadwisp.seguridad.Rol
import mx.com.wisnet.sadwisp.seguridad.Usuario
import mx.com.wisnet.sadwisp.seguridad.UsuarioRol

import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.TemporalAdjusters

import static org.springframework.http.HttpStatus.*

class TicketController {

    TicketService ticketService
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {

        def usuario = springSecurityService.currentUser
        def listaTickets = new ArrayList()
        if(usuario.getRole().nombre.equals('ROLE_ADMINISTRADOR')){
            LocalDate fecha = LocalDate.now();
            def dateInit  = fecha.with(TemporalAdjusters.firstDayOfMonth());
            def dateEnd = fecha.with(TemporalAdjusters.lastDayOfMonth());
            //Ticket.findAllByDateCreatedBetween(dateInit, dateEnd)
            ZoneId defaultZoneId = ZoneId.systemDefault();
            listaTickets = Ticket.findAllByDateCreatedBetween(Date.from(dateInit.atStartOfDay(defaultZoneId).toInstant()), Date.from(dateEnd.atStartOfDay(defaultZoneId).toInstant()))
            //listaTickets = Ticket.list(params)
        }else{

            def lista = Ticket.list()
            lista.each {it->
                if(it.tecnicos.contains(usuario)&& !it.status.equals("CERRADO")){
                    listaTickets.add(it)
                }
            }
        }


        respond listaTickets,  model:[ticketCount: listaTickets.size()]
    }


    def create() {
        def clienteList = Cliente.list()
        def role = Rol.findAllByNombre('ROLE_TECNICO')
        def lista = UsuarioRol.findAllByRol(role)
        def tecnicoList = new ArrayList()
        lista.each {it->
            tecnicoList.add(it.usuario)
        }
        respond new Ticket(params), model:[clienteList: clienteList, tecnicoList:tecnicoList]
    }

    def show(Long id) {
        respond ticketService.get(id)
    }

    def save(Ticket ticket) {
        if (ticket == null) {
            notFound()
            return
        }

        ticket.status = 'ASIGNADO'
        ticket.userCreated = springSecurityService.currentUser
        ticket.userUpdated = springSecurityService.currentUser

        try {
            ticketService.save(ticket)
        } catch (ValidationException e) {
            respond ticket.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'configuracion.label', default: 'Ticket'), ticket.id])
                redirect(action: 'index')
            }
            '*' { respond index, [status: CREATED] }
        }
    }

    def saveIncident(Incidencia incidencia){
        if (incidencia == null) {
            notFound()
            return
        }

        def ticket = Ticket.get(params.ticketId)
        incidencia.ticket = ticket
        incidencia.userUpdated = springSecurityService.currentUser
        incidencia.userCreated = springSecurityService.currentUser

        try {
            incidencia.save()
        } catch (ValidationException e) {
            respond incidencia.errors, view:'create'
            return
        }


        redirect(action: 'index')

    }

    def saveClose(Ticket ticket) {
        if (ticket == null) {
            notFound()
            return
        }

        ticket.status = 'CERRADO'
        ticket.userUpdated = springSecurityService.currentUser

        try {
            ticketService.save(ticket)
        } catch (ValidationException e) {
            respond ticket.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'configuracion.label', default: 'Ticket'), ticket.id])
                redirect(action: 'index')
            }
            '*' { respond index, [status: CREATED] }
        }
    }

    def cerrar(Ticket ticket){
        if (ticket == null) {
            notFound()
            return
        }

        respond ticket
    }

    def incident(Ticket ticket){
        if (ticket == null) {
            notFound()
            return
        }

        def incidencia = new Incidencia()
        incidencia.userUpdated = springSecurityService.currentUser
        incidencia.userCreated = springSecurityService.currentUser
        incidencia.ticket = ticket


        respond incidencia
    }


    def procesar(Ticket ticket) {
        if (ticket == null) {
            notFound()
            return
        }

        ticket.status = 'EN PROCESO'
        try {
            ticketService.save(ticket)
        } catch (ValidationException e) {
            respond historial.errors, view:'edit'
            return
        }

        redirect(action:'index')

    }
}
