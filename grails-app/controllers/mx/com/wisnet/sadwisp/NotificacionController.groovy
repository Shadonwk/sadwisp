package mx.com.wisnet.sadwisp

import grails.validation.ValidationException
import mx.com.wisnet.sadwisp.dto.MessageSMSDTO
import org.grails.web.json.JSONArray
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

import static org.springframework.http.HttpStatus.*

class NotificacionController {

    NotificacionService notificacionService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def cliente = Cliente.get(1)
        //sendSuspendServiceMessage(cliente)
        respond notificacionService.list(params), model:[notificacionCount: notificacionService.count()]
    }

    def show(Long id) {
        respond notificacionService.get(id)
    }

    def create() {
        respond new Notificacion(params)
    }

    def save(Notificacion notificacion) {
        if (notificacion == null) {
            notFound()
            return
        }

        try {
            notificacionService.save(notificacion)
        } catch (ValidationException e) {
            respond notificacion.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'notificacion.label', default: 'Notificacion'), notificacion.id])
                redirect notificacion
            }
            '*' { respond notificacion, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond notificacionService.get(id)
    }

    def update(Notificacion notificacion) {
        if (notificacion == null) {
            notFound()
            return
        }

        try {
            notificacionService.save(notificacion)
        } catch (ValidationException e) {
            respond notificacion.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'notificacion.label', default: 'Notificacion'), notificacion.id])
                redirect notificacion
            }
            '*'{ respond notificacion, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        notificacionService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'notificacion.label', default: 'Notificacion'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'notificacion.label', default: 'Notificacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }


}
