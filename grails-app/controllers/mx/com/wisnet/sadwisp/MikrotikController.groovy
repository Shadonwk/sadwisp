package mx.com.wisnet.sadwisp

import grails.validation.ValidationException
import me.legrange.mikrotik.ApiConnection

import static org.springframework.http.HttpStatus.*

class MikrotikController {

    MikrotikService mikrotikService
    MikrotikHelperService mikrotikHelperService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        respond mikrotikService.list(params), model:[mikrotikCount: mikrotikService.count()]
    }

    def show(Long id) {


        def mikrotik = mikrotikService.get(id)
        def detalle = mikrotikHelperService.detalles(mikrotik)
        respond mikrotikService.get(id), model:[detalles:detalle]
    }

    def create() {
        //validar licencia
        def total = Mikrotik.count()
        //if (total<1){
            respond new Mikrotik(params)
        //}else{
        //    flash.message = message(code: 'lisencia.max.created.message')
        //    redirect(action: 'index')
        //}


    }

    def save(Mikrotik mikrotik) {
        if (mikrotik == null) {
            notFound()
            return
        }

        try {
            mikrotikService.save(mikrotik)
        } catch (ValidationException e) {
            respond mikrotik.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mikrotik.label', default: 'Mikrotik'), mikrotik.id])
                redirect mikrotik
            }
            '*' { respond mikrotik, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond mikrotikService.get(id)
    }

    def update(Mikrotik mikrotik) {
        if (mikrotik == null) {
            notFound()
            return
        }

        try {
            mikrotikService.save(mikrotik)
        } catch (ValidationException e) {
            respond mikrotik.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'mikrotik.label', default: 'Mikrotik'), mikrotik.id])
                redirect mikrotik
            }
            '*'{ respond mikrotik, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        mikrotikService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'mikrotik.label', default: 'Mikrotik'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mikrotik.label', default: 'Mikrotik'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def importarPPPOE(Long id){
        def mikrotik = mikrotikService.get(id)
        def exito = false

        exito = mikrotikHelperService.importarClientesPPPOE(mikrotik)

        if (exito){
            flash.message="Se realizo la importación de los PPPoE correctamente"
            redirect action: 'show', id: id
            return
        }else{
            flash.message = "Ha ocurrido un error intente más tarde"
            redirect action: 'show', id: id
            return
        }

    }

    def importarClientes(Long id){
        def mikrotik = mikrotikService.get(id)
        def exito = false

        exito = mikrotikHelperService.importarClientes(mikrotik)

        if (exito){
            flash.message="Se realizo la importación de las queues correctamente"
            redirect action: 'show', id: id
            return
        }else{
            flash.message = "Ha ocurrido un error intente más tarde"
            redirect action: 'show', id: id
            return
        }



    }
}
