package mx.com.wisnet.sadwisp

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ConfiguracionController {

    ConfiguracionService configuracionService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond configuracionService.list(params), model:[configuracionCount: configuracionService.count()]
    }

    def show(Long id) {
        respond configuracionService.get(id)
    }

    def create() {
        respond new Configuracion(params)
    }

    def save(Configuracion configuracion) {
        if (configuracion == null) {
            notFound()
            return
        }



        try {
            configuracionService.save(configuracion)
        } catch (ValidationException e) {
            respond configuracion.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'configuracion.label', default: 'Configuracion'), configuracion.id])
                redirect configuracion
            }
            '*' { respond configuracion, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond configuracionService.get(id)
    }

    def update(Configuracion configuracion) {
        if (configuracion == null) {
            notFound()
            return
        }

        try {
            configuracionService.save(configuracion)
        } catch (ValidationException e) {
            respond configuracion.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'configuracion.label', default: 'Configuracion'), configuracion.id])
                redirect (action: 'edit', id:configuracion.id)
            }
            '*'{ respond configuracion, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        configuracionService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'configuracion.label', default: 'Configuracion'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'configuracion.label', default: 'Configuracion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
