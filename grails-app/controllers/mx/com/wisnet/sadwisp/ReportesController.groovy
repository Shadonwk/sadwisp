package mx.com.wisnet.sadwisp

import mx.com.wisnet.sadwisp.seguridad.Usuario

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters
import java.util.stream.IntStream

class ReportesController {

    def index() {

        def usuarios =Usuario.list()
        [usuarios:usuarios]
    }

    def generarReporte(){


        LocalDate fechaInicio = new LocalDate(params.fechaInicio_year.toInteger(), params.fechaInicio_month.toInteger(),
                params.fechaInicio_day.toInteger());
        LocalDate fechaFin = new LocalDate(params.fechaFin_year.toInteger(), params.fechaFin_month.toInteger(),
                params.fechaFin_day.toInteger());
        def usuario = Usuario.get(params.usuario)

        def clientesLinea = 0
        def clientesSuspendidos = 0
        def pagosHoy = 0
        def pagosMes = 0
        def paramsQuery = [sort: "fechaPago", order: "desc"]
        def pagos = Historial.findAllByFechaPagoBetweenAndUsuarioCobro(fechaInicio, fechaFin, usuario, paramsQuery)
        def usuarios =Usuario.list()
        def sumatoria =  (pagos.stream().mapToDouble {i-> i.cantidad == null ? new Double(0) : i.cantidad}.sum())


        render view: 'index', model: [pagos:pagos, usuarios:usuarios,usuario:usuario,
                                      fechaInicio:params.fechaInicio, fechaFin:params.fechaFin, sumatoria:sumatoria]
    }
}
