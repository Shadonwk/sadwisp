package mx.com.wisnet.sadwisp

import grails.converters.JSON
import mx.com.wisnet.sadwisp.seguridad.Usuario

import java.time.LocalDate
import java.time.Month
import java.time.Period
import java.time.temporal.TemporalAdjusters


class Api2Controller {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def clienteService
    def historialService
    def mikrotikHelperService
    def springSecurityService

    def getPlanes(){
        println ("Entra a los planes...")
        def planes = Plan.list()
        response.setContentType("application/json;charset=UTF-8")
        JSON.use('planes'){
            render planes as JSON
        }

    }

    def getMikrotiks(){
        def mikrotiks = Mikrotik.list()
        response.setContentType("application/json;charset=UTF-8")
        JSON.use('mikrotiks'){
            render mikrotiks as JSON
        }

    }

    def getListaClientes(){
    
        println "Se invoca la lista de cientes"

        def clientes = Cliente.list()
        response.setContentType("application/json;charset=UTF-8")
        render clientes as JSON


//        def payload = "[\n" +
//                "    {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"Roberto\",\n" +
//                "        \"aPaterno\": \"León\",\n" +
//                "        \"aMaterno\": \"Cruz\",\n" +
//                "        \"direccion\": \"Bomintzhá\"\n" +
//
//                "    },\n"+
//                "    {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"Carlos\",\n" +
//                "        \"aPaterno\": \"León\",\n" +
//                "        \"aMaterno\": \"Cruz\",\n" +
//                "        \"direccion\": \"Bomintzhá\"\n" +
//
//                "    },\n"+
//        "    {\n" +
//                "        \"id\": 3,\n" +
//                "        \"nombre\": \"Mario\",\n" +
//                "        \"aPaterno\": \"León\",\n" +
//                "        \"aMaterno\": \"López\",\n" +
//                "        \"direccion\": \"Bomintzhá\"\n" +
//
//                "    }\n"+
//
//
//                "]"
//        response.setContentType("application/json;charset=UTF-8")
//        render payload
    }

    def saveCliente(){
        def current = springSecurityService.currentUser

        def final request2 = request.JSON

        Cliente cliente;
        if (Integer.parseInt(String.valueOf(request2.get("id")))!=0){
            println ("actualizar")
            cliente = Cliente.get(Integer.parseInt(String.valueOf(request2.get("id"))));
        }else{
            println ("crear nuevo cliente")
            cliente = new Cliente()
            println("cliente id_1 = "+ cliente.id)
        }


        cliente.nombre = request2.get("nombre")
        cliente.apellidoPaterno = request2.get("apellidoPaterno")
        cliente.apellidoMaterno = request2.get("apellidoMaterno")
        cliente.direccion = request2.get("direccion")
        cliente.email = request2.get("nombre")+request2.get("apellidoPaterno")+"@gmail.com" //request2.get("email")
        cliente.telefono = request2.get("telefono")
        cliente.caja = request2.get("caja")
        cliente.comentarios = request2.get("comentarios")
        cliente.diaCorte = request2.get("diaCorte")
        cliente.name = request2.get("name")
        cliente.ip = request2.get("ip")
        cliente.macAntena = null //request2.get("macAntena")
        cliente.plan = Plan.get(request2.get("plan"))
        cliente.mikrotik = Mikrotik.get(request2.get("mikrotik"))
        cliente.statusServicio = request2.get("statusServicio")

        println("cliente id_2 = "+ cliente.id)
        if(cliente.validate()) {
            println("cliente id_3 = "+ cliente.id)
            clienteService.save(cliente)
            generateHistorial(cliente)
            response.status = 200
        }else{
            print cliente.errors
            response.status = 400
        }


    }

    def getHistorial(int id){
        def final request2 = request.JSON
        println("request" + request2)
        println("id" + id)
        def cliente = Cliente.get(id)
        def historialList = Historial.findAllByCliente(cliente)
        historialList.sort()
        JSON.use('historial') {
            print historialList as JSON
            render historialList as JSON
        }
    }


    def generateHistorial(Cliente cliente) {

        LocalDate dt = LocalDate.now();
        String month = dt.month;  // where January is 1 and December is 12
        int year = dt.year;

        LocalDate lastDayOfYear = LocalDate.of(year, Month.DECEMBER,31)

        Period period = dt.until(lastDayOfYear);


        def months = period.getMonths() + 1
        months.times {
            def historial = new Historial()
            historial.setCliente(cliente)
            historial.setMes(dt.plusMonths(it).getMonthValue()) //todo guardar mes actual
            historial.setAnio(year) //todo guardar año acutal
            historial.setStatusPago(false) //para la primera vez siempre sera pendiente
            historialService.save(historial)
        }
    }

    def registrarPago(int idHistorial){

        def historial = Historial.get(idHistorial)
        historial.statusPago = true
        historial.fechaPago = LocalDate.now()
        def usuarioActual = springSecurityService.currentUser
        historial.usuarioCobro = usuarioActual
        //relizar la activación del cliente
        def cliente= historial.cliente

        def respuesta =1

        if (cliente.statusServicio.equals("SUSPENDIDO")){
            respuesta = mikrotikHelperService.reactivarServicio(cliente)
        }

        if(respuesta==0){
            // no se pudo activar intente mas tarde
            response.status = 400
        }else{
            cliente.statusServicio= "EN_LINEA"
            clienteService.save(cliente)
            historialService.save(historial)
            response.status = 200
        }

    }

    def getResumen(){

        def clientesLinea = 0
        def clientesSuspendidos = 0
        def pagosHoy = 0
        def pagosMes = 0


        clientesLinea = Cliente.findAllByStatusServicio("EN_LINEA").size()
        clientesSuspendidos = Cliente.findAllByStatusServicio("SUSPENDIDO").size()

        def pagos = Historial.findAllByFechaPago(LocalDate.now())
        pagos.each {
            def cliente = it.cliente
            pagosHoy += cliente.plan.costo
        }

        def fecha = LocalDate.now()
        def ultimoDiaMes = fecha.with(TemporalAdjusters.lastDayOfMonth())
        def primerDiaMes = fecha.with(TemporalAdjusters.firstDayOfMonth())
        def pagosM = Historial.findAllByFechaPagoBetween(primerDiaMes, ultimoDiaMes)

        pagosM.each {
            pagosMes+= it.cliente.plan.costo
        }

        def usuarios = Usuario.findAllByEnabled(true)
        def resumen = [:]

        usuarios.each {
            def totalCobros = 0
            def registros  = Historial.findAllByFechaPagoBetweenAndUsuarioCobro(primerDiaMes, ultimoDiaMes, it)
            if(!registros.isEmpty())
                registros.each {
                    totalCobros += it.cliente.plan.costo
                }
            resumen[it.nombreCompleto] = '$'+totalCobros
        }


        def mapa = [:]

        mapa["clientesLinea"] = clientesLinea
        mapa["clientesSuspendidos"]=clientesSuspendidos
        mapa["pagosHoy"]= pagosHoy
        mapa["pagosMes"] = pagosMes
        mapa["resumen"]= resumen

        render mapa as JSON
    }


}