package mx.com.wisnet.sadwisp

import com.itextpdf.text.BaseColor
import com.itextpdf.text.Chunk
import com.itextpdf.text.Document
import com.itextpdf.text.Element
import com.itextpdf.text.Font
import com.itextpdf.text.Image
import com.itextpdf.text.PageSize
import com.itextpdf.text.Paragraph
import com.itextpdf.text.Phrase
import com.itextpdf.text.Rectangle
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.lowagie.text.FontFactory
import com.lowagie.text.pdf.PdfCell
import com.lowagie.text.pdf.PdfPTableEvent
import grails.plugins.jasper.JasperExportFormat
import grails.plugins.jasper.JasperReportDef
import grails.plugins.jasper.JasperService
import grails.validation.ValidationException
import me.legrange.mikrotik.ApiConnection
import mx.com.wisnet.sadwisp.util.Converter
import net.sf.jasperreports.engine.JasperReport
import org.grails.io.support.ByteArrayResource
import org.springframework.core.io.InputStreamSource

import javax.print.Doc
import javax.print.DocFlavor
import javax.print.DocPrintJob
import javax.print.PrintService
import javax.print.PrintServiceLookup
import javax.print.SimpleDoc
import javax.print.attribute.Attribute
import javax.print.attribute.AttributeSet
import javax.print.attribute.HashAttributeSet
import javax.print.attribute.PrintServiceAttributeSet
import javax.print.attribute.standard.Destination
import javax.print.attribute.standard.PrinterInfo
import javax.print.attribute.standard.PrinterIsAcceptingJobs
import javax.print.attribute.standard.PrinterLocation
import javax.print.attribute.standard.PrinterMakeAndModel
import javax.print.attribute.standard.PrinterName
import javax.print.attribute.standard.PrinterState
import javax.servlet.ServletOutputStream
import java.awt.Color
import java.time.LocalDate
import java.time.Month
import java.time.Period
import java.time.format.TextStyle

import static org.springframework.http.HttpStatus.*

class HistorialController {

    HistorialService historialService
    ClienteService clienteService
    ApiConnection con
    def assetResourceLocator
    def configuracionService
    //def nine.jasper.JasperService jasperService
    def springSecurityService
    def mailService

    JasperService jasperService

    MikrotikHelperService mikrotikHelperService


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond historialService.list(params), model:[historialCount: historialService.count()]
    }

    def show(Long id) {

        def cliente = Cliente.get(id)

        def historialList = Historial.findAllByCliente(cliente)
        historialList.sort()
       def editarNota = Configuracion.get(1).editarNota

        [cliente:cliente.nombreCompleto, historialList:historialList, editarNota:editarNota]
    }

    def create() {
        respond new Historial(params)
    }

    def save(Historial historial) {
        if (historial == null) {
            notFound()
            return
        }

        try {
            historialService.save(historial)
        } catch (ValidationException e) {
            respond historial.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'historial.label', default: 'Historial'), historial.id])
                redirect historial
            }
            '*' { respond historial, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond historialService.get(id)
    }

    def update(Historial historial) {
        if (historial == null) {
            notFound()
            return
        }

        try {
            historialService.save(historial)
        } catch (ValidationException e) {
            respond historial.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'historial.label', default: 'Historial'), historial.id])
                redirect historial
            }
            '*'{ respond historial, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        historialService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'historial.label', default: 'Historial'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'historial.label', default: 'Historial'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def sendPayment(Long id){

        def historial = Historial.get(id)
        historial.statusPago = true
        historial.fechaPago = LocalDate.now()
        historial.usuarioCobro = springSecurityService.currentUser
        historial.cantidad = historial.cliente.plan.costo

        //relizar la activación del cliente
        def cliente= historial.cliente
        def conexion = cliente.mikrotik

        if (cliente.statusServicio.equals("SUSPENDIDO")){
            mikrotikHelperService.reactivarServicio(cliente)
        }
        cliente.statusServicio= "EN_LINEA"
        clienteService.save(cliente)
        historialService.save(historial)
        redirect(action: 'show', id:historial.cliente.id)
    }

    public static void printAvailable() {

        // busca los servicios de impresion...
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);

        // -- ver los atributos de las impresoras...
        for (PrintService printService : services) {

            System.out.println(" ---- IMPRESORA: " + printService.getName());

            PrintServiceAttributeSet printServiceAttributeSet = printService.getAttributes();

            System.out.println("--- atributos");

            // todos los atributos de la impresora
            Attribute[] a = printServiceAttributeSet.toArray();
            for (Attribute unAtribute : a) {
                System.out.println("atributo: " + unAtribute.getName());
            }

            System.out.println("--- viendo valores especificos de los atributos ");

            // valor especifico de un determinado atributo de la impresora
            System.out.println("PrinterLocation: " + printServiceAttributeSet.get(PrinterLocation.class));
            System.out.println("PrinterInfo: " + printServiceAttributeSet.get(PrinterInfo.class));
            System.out.println("PrinterState: " + printServiceAttributeSet.get(PrinterState.class));
            System.out.println("Destination: " + printServiceAttributeSet.get(Destination.class));
            System.out.println("PrinterMakeAndModel: " + printServiceAttributeSet.get(PrinterMakeAndModel.class));
            System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributeSet.get(PrinterIsAcceptingJobs.class));

        }

    }


    def printByName(String printName, String nameLocal, String expedition, String box, String ticket, String caissier, String dateTime, String items, String subTotal, String tax, String total, String recibo, String change) {

        String contentTicket = "VINATERIA {{nameLocal}}\n"+
                "EXPEDIDO EN: {{expedition}}\n"+
                "DOMICILIO CONOCIDO MERIDA, YUC.\n"+
                "=============================\n"+
                "MERIDA, XXXXXXXXXXXX\n"+
                "RFC: XXX-020226-XX9\n"+
                "Caja # {{box}} - Ticket # {{ticket}}\n"+
                "LE ATENDIO: {{cajero}}\n"+
                "{{dateTime}}\n"+
                "=============================\n"+
                "{{items}}\n"+
                "=============================\n"+
                "SUBTOTAL: {{subTotal}}\n"+
                "IVA: {{tax}}\n"+
                "TOTAL: {{total}}\n\n"+
                "RECIBIDO: {{recibo}}\n"+
                "CAMBIO: {{change}}\n\n"+
                "=============================\n"+
                "GRACIAS POR SU COMPRA...\n"+
                "ESPERAMOS SU VISITA NUEVAMENTE {{nameLocal}}\n"+
                "\n"+
                "\n";

        contentTicket = contentTicket.replace("{{nameLocal}}", nameLocal);
        contentTicket = contentTicket.replace("{{expedition}}", expedition);
        contentTicket = contentTicket.replace("{{box}}", box);
        contentTicket = contentTicket.replace("{{ticket}}", ticket);
        contentTicket = contentTicket.replace("{{cajero}}", caissier);
        contentTicket = contentTicket.replace("{{dateTime}}", dateTime);
        contentTicket = contentTicket.replace("{{items}}", items);
        contentTicket = contentTicket.replace("{{subTotal}}", subTotal);
        contentTicket = contentTicket.replace("{{tax}}", tax);
        contentTicket = contentTicket.replace("{{total}}", total);
        contentTicket = contentTicket.replace("{{recibo}}", recibo);
        contentTicket = contentTicket.replace("{{change}}", change);

        //Especificamos el tipo de dato a imprimir
        //Tipo: bytes; Subtipo: autodetectado
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE
        InputStream is = new ByteArrayInputStream("hello world!\f".getBytes("UTF8"));


        PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);

        // buscar por el nombre de la impresora (nombre que le diste en tu S.O.)
        // en "aset" puedes agregar mas atributos de busqueda
        AttributeSet aset = new HashAttributeSet();
        aset.add(new PrinterName(printName, null));
        //aset.add(ColorSupported.SUPPORTED); // si quisieras buscar ademas las que soporten color

        services = PrintServiceLookup.lookupPrintServices(flavor, aset);
        if(services.length == 0){
            System.out.println("No se encontro impresora con nombre " + printName);
        }
        for (PrintService printService : services) {
            System.out.println(printService.getName());
        }

        //Creamos un arreglo de tipo byte
        byte[] bytes;

        //Aca convertimos el string(cuerpo del ticket) a bytes tal como
        //lo maneja la impresora(mas bien ticketera :p)
        bytes = contentTicket.getBytes();

        //Creamos un documento a imprimir, a el se le appendeara
        //el arreglo de bytes
        String hola = "Hola mundo de las impresoras pedorras..."
        Doc doc = new SimpleDoc(hola.getBytes('CP437'),flavor,null);

        //Creamos un trabajo de impresión
        DocPrintJob job = services[0].createPrintJob();

        //Imprimimos dentro de un try de a huevo
        try {
            //El metodo print imprime
            job.print(doc, null);
        } catch (Exception er) {
            println(null,"Error al imprimir: " + er.getMessage());
        }

    }

    def printNote(Long id){

        //printAvailable()
        //printByName("Brother MFC-J480DW","roberto", "Bomintzhá de tula de allende", "1","111", "Roberto León","10/10/2019", "items", "200", "2.0", "202.00", "recibo", "change")

        def historial = Historial.get(id)
        def cliente = historial.cliente

        //Document documento = new Document(PageSize.A4, 25, 25, 25, 25);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //PdfWriter writer = PdfWriter.getInstance(documento, out);

        //Resource recurso = assetResourceLocator.findResourceForURI("reports")
        Document document = new Document(PageSize.LETTER, 25, 25, 25, 25);
       // def pdfwriter = PdfWriter.getInstance(document, new FileOutputStream(path + "nota33.pdf"));
        def pdfwriter = PdfWriter.getInstance(document, out);
        document.open();
        Font f = new Font(Font.FontFamily.TIMES_ROMAN, 25.0f, Font.BOLD, BaseColor.BLACK);
        Chunk c = new Chunk("Hola esta es una prueba", f);
        c.setBackground(BaseColor.RED);
        Paragraph p = new Paragraph(c);





        System.out.println("La configuracion para notas es: " +Configuracion.get(1).editarNota )

        def config = Configuracion.get(1)
        def numero = "No.: "+ config.consecutivoNotas
        def nombreCompleto= cliente.nombreCompleto
        if(config.editarNota)
            nombreCompleto=params.name



        LocalDate dt = LocalDate.now();
        LocalDate dt2 = LocalDate.now()

        String month = dt.month.getDisplayName(TextStyle.FULL, new Locale("es","MX"));  // where January is 1 and December is 12
        def day = dt.dayOfMonth
        int year = dt.year;
        def fecha = Configuracion.get(1).direccionFecha + " a $day de $month de $year"
        def granTotal = "\$" + cliente.plan.costo
        int valor = cliente.plan.costo.intValue()
        def totalLetra = "("+new Converter().convertirLetras(valor) + " pesos 00/M.N.)"


        def precioUnitario = "\$ $cliente.plan.costo"
        dt2 = dt2.minusMonths(1)
        String mesAnterior = dt2.month.getDisplayName(TextStyle.FULL, new Locale("es","MX"));  // where January is 1 and December is 12
        if(config.isPostPago()){
            month = mesAnterior
        }
        def descripcion = "Internet simétrico correspondiente al mes de $month con plan: $cliente.plan"
        document.add(generateTableGenerica("odan", cliente, nombreCompleto, numero, fecha, granTotal, totalLetra, descripcion, precioUnitario, config ))

        document.close();


        log.debug "Entra a generar el reporte "

        def configuracion = Configuracion.get(1)
        configuracion.consecutivoNotas = configuracion.consecutivoNotas + 1
        configuracionService.save(configuracion)
        response.setContentType('application/pdf')
        response.addHeader('Content-Disposition', "attachment;filename=nota.pdf")
        response.setStatus(response.SC_OK)
        //response.outputStream<<out
        //response.outputStream.flush()

        ServletOutputStream out2 = response.getOutputStream();
        //out.writeTo(out2);
        //out.flush();



        if(cliente.email){
            mailService.sendMail {
                multipart true
                to cliente.email
                from "test@treisatransportes.com"
                subject "Sistema de administración de clientes"
                text "Estimado cliente ${cliente.nombreCompleto} se ha generado la nota correspondiente al mes $month \n\nEste mensaje se ha generado automáticamente no es necesario que lo responda."
                attach "nota.pdf", "application/pdf", out.toByteArray()
            }
        }


        response.flushBuffer();
    }



    def generateTableGenerica(logo, cliente, nombreCompleto,  noRecibo, fechaS, granTotal,
                      totalLetra, descripcion, precioUnitario,Configuracion config  ){

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);

        PdfPCell cellEmpty = new PdfPCell(new Phrase("  "));
        cellEmpty.setColspan(6)

        cellEmpty.setBorder(Rectangle.NO_BORDER);
        cellEmpty.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellEmpty.setHorizontalAlignment(Element.ALIGN_CENTER);

        Font smallfont = new Font(Font.FontFamily.HELVETICA, 30);
        Font titleFont = new Font(Font.FontFamily.COURIER, 19);

        PdfPCell cell = new PdfPCell(new Phrase("  "));
        cell.setColspan(6)
        cell.setBackgroundColor(new BaseColor(37,36,64));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell)
        table.addCell(cell)

        table.addCell(cellEmpty)

        def image = Image.getInstance("/tmp/assets/logos/$config.nombreLogo")
        def imageQR = null
        def imageBarcode = null
        if(config.qrCode!=null) imageQR = Image.getInstance("/tmp/assets/qrcode/$config.qrCode")
        if(config.barCode!=null) imageBarcode = Image.getInstance("/tmp/assets/barcode/$config.barCode")
        //def imagePinoJPG = Image.getInstance("/tmp/assets/pino2.jpg")
        PdfPCell nombreEmpresa = new PdfPCell( new Phrase(config.nombreEmpresa, titleFont))

        /*if(logo.equals("odan")){
            image = Image.getInstance("/tmp/assets/odanLogo.jpeg")
            imageQR = Image.getInstance("/tmp/assets/odanQrCode.jpeg")
            nombreEmpresa = new PdfPCell( new Phrase("GRUPO ODAN RED", titleFont))
            imageBarcode = Image.getInstance("/tmp/assets/barcode/OdanBarcode.jpeg")
        }*/

        //image.setWidthPercentage(50)
        //image.scaleAbsolute(250, 60);
        PdfPCell imageCell = new PdfPCell(image, true);
        //imageCell.addElement(,true);
        imageCell.setColspan(3);
        imageCell.setRowspan(6);
        imageCell.setBorder(Rectangle.NO_BORDER);



        nombreEmpresa.setColspan(3)
        nombreEmpresa.setBorder(Rectangle.NO_BORDER);

        PdfPCell servicioNet = new PdfPCell( new Phrase("SERVICIO DE INTERNET", titleFont))
        servicioNet.setColspan(3)
        servicioNet.setBorder(Rectangle.NO_BORDER);

        PdfPCell permiso = new PdfPCell(new Phrase("N/A"))
        if(config.permisoIft!=null){
            permiso = new PdfPCell(new Phrase(config?.permisoIft))
            permiso.setColspan(3)
            permiso.setBorder(Rectangle.NO_BORDER)
        }

        //PdfPCell direccion = new PdfPCell(new Phrase("JOSEFA ORTIZ DE DOMINGUEZ 79"))
        PdfPCell direccion = new PdfPCell(new Phrase(config.direccion))
        direccion.setColspan(3)
        direccion.setBorder(Rectangle.NO_BORDER);


        //PdfPCell codigoPostal = new PdfPCell(new Phrase("CP.:16450"))
        PdfPCell codigoPostal = new PdfPCell(new Phrase("CP.: " + config.codigoPostal))
        codigoPostal.setColspan(3)
        codigoPostal.setBorder(Rectangle.NO_BORDER);

        //PdfPCell telefonoNegocio = new PdfPCell(new Phrase("TEL. 7701092906"))
        PdfPCell telefonoNegocio = new PdfPCell(new Phrase("TEL.: " + config.telefono))
        telefonoNegocio.setColspan(3)
        telefonoNegocio.setBorder(Rectangle.NO_BORDER);


        table.addCell(nombreEmpresa)
        table.addCell(imageCell);

        table.addCell(servicioNet)
        table.addCell(permiso)
        table.addCell(direccion)
        table.addCell(codigoPostal)
        table.addCell(telefonoNegocio)


        table.addCell(cellEmpty)


        PdfPCell a = new PdfPCell(new Phrase("A:"))
        a.setColspan(3)
        a.setBorder(Rectangle.NO_BORDER);


        PdfPCell numero = new PdfPCell(new Phrase("Recibo  $noRecibo"))
        numero.setColspan(3)
        numero.setBorder(Rectangle.NO_BORDER);
        numero.setHorizontalAlignment(Element.ALIGN_RIGHT)

        PdfPCell nombreCliente = new PdfPCell(new Phrase(nombreCompleto))
        nombreCliente.setColspan(3)
        nombreCliente.setBorder(Rectangle.NO_BORDER);

        PdfPCell fecha = new PdfPCell(new Phrase(fechaS))
        fecha.setColspan(3)
        fecha.setRowspan(3)
        fecha.setHorizontalAlignment(Element.ALIGN_RIGHT)
        fecha.setBorder(Rectangle.NO_BORDER);

        PdfPCell direccionCliente = new PdfPCell(new Phrase(cliente.direccion))
        direccionCliente.setColspan(3)
        direccionCliente.setBorder(Rectangle.NO_BORDER);

        table.addCell(a)
        table.addCell(numero)
        table.addCell(nombreCliente)
        table.addCell(fecha)
        table.addCell(direccionCliente)



        if(cliente.telefono!=null){
            PdfPCell telefono = new PdfPCell(new Phrase(cliente.telefono))
            telefono.setColspan(3)
            telefono.setBorder(Rectangle.NO_BORDER);
            table.addCell(telefono)
        }

        table.addCell(cellEmpty)


        PdfPCell totalString = new PdfPCell(new Phrase(" T  O  T  A  L", smallfont))
        totalString.setColspan(3)
        totalString.setFixedHeight(40)
        totalString.setVerticalAlignment(Element.ALIGN_CENTER)
        totalString.setBorder(Rectangle.BOTTOM|Rectangle.TOP);

        PdfPCell totalMonto = new PdfPCell(new Phrase(granTotal,smallfont))
        totalMonto.setColspan(3)
        totalMonto.setHorizontalAlignment(Element.ALIGN_RIGHT)
        totalMonto.setVerticalAlignment(Element.ALIGN_CENTER)
        totalMonto.setBorder(Rectangle.BOTTOM| Rectangle.TOP);


        table.addCell(totalString)
        table.addCell(totalMonto)

        table.addCell(cellEmpty)

        PdfPCell descripcionS = new PdfPCell(new Phrase("D E S C R I P C I Ó N"))
        descripcionS.setColspan(3)
        descripcionS.setBorder(Rectangle.NO_BORDER);

        PdfPCell descripcionServicio = new PdfPCell(new Phrase(descripcion))
        descripcionServicio.setColspan(3)
        descripcionServicio.setBorder(Rectangle.NO_BORDER);

        PdfPCell subtotalS = new PdfPCell(new Phrase("I M P O R T E"))
        subtotalS.setColspan(3)
        subtotalS.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotalS.setBorder(Rectangle.NO_BORDER);


        PdfPCell subtotal = new PdfPCell(new Phrase(granTotal))
        subtotal.setColspan(3)
        subtotal.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotal.setBorder(Rectangle.NO_BORDER);


        table.addCell(descripcionS)
        table.addCell(subtotalS)
        //table.addCell(cellEmpty)
        table.addCell(descripcionServicio)
        table.addCell(subtotal)

        //table.addCell(cellEmpty)

        PdfPCell totalLetraCell = new PdfPCell(new Phrase(totalLetra))
        totalLetraCell.setColspan(6)
        totalLetraCell.setBorder(Rectangle.NO_BORDER);

        table.addCell(totalLetraCell)
        //table.addCell(cellEmpty)
        //table.addCell(cellEmpty)

        PdfPCell diaPago = new PdfPCell(new Phrase("Día de pago: " +cliente.diaCorte +" de cada mes."))
        diaPago.setColspan(3)
        //diaPago.setHorizontalAlignment(Element.ALIGN_RIGHT)
        diaPago.setBorder(Rectangle.NO_BORDER);

        table.addCell(diaPago)
        table.addCell(cellEmpty)
        table.addCell(cellEmpty)


        PdfPCell condiciones = new PdfPCell(new Phrase("CONDICIONES Y FORMA DE PAGO"))
        condiciones.setColspan(6)
        condiciones.setBorder(Rectangle.NO_BORDER);

        table.addCell(condiciones)

        PdfPCell condicionesString = new PdfPCell(new Phrase("TIEMPO LIMITE DE PAGO $config.diasGraciaCorte DIAS HABILES DESPUES DE LA FECHA INDICADA"))
        condicionesString.setColspan(6)
        condicionesString.setBorder(Rectangle.NO_BORDER);

        table.addCell(condicionesString)

        table.addCell(cellEmpty)


        Font avisoFont = new Font(Font.FontFamily.HELVETICA, 20);
        avisoFont.setColor(13,147,15)

        PdfPCell nuevoCosto = new PdfPCell(new Phrase(config.aviso, avisoFont))
        nuevoCosto.setColspan(6)
        nuevoCosto.setBorder(Rectangle.NO_BORDER);
        nuevoCosto.setHorizontalAlignment(Element.ALIGN_CENTER)
        table.addCell(nuevoCosto)


        table.addCell(cellEmpty)
        table.addCell(cellEmpty)

        /*imagePinoJPG.scaleAbsolute(100, 100);
        PdfPCell imagePino = new PdfPCell(imagePinoJPG, false);
        //imageQRCode.addElement();
        imagePino.setColspan(6);
        //imageQRCode.setRowspan(3);
        imagePino.setHorizontalAlignment(Element.ALIGN_CENTER)
        imagePino.setBorder(Rectangle.NO_BORDER);

        table.addCell(imagePino)
        */
        if(config.qrCode!=null){
            imageQR.scaleAbsolute(50, 50);

            PdfPCell imageQRCode = new PdfPCell(imageQR, false);
            //imageQRCode.addElement();
            imageQRCode.setColspan(3);
            //imageQRCode.setRowspan(3);
            imageQRCode.setHorizontalAlignment(Element.ALIGN_LEFT)
            imageQRCode.setBorder(Rectangle.NO_BORDER);

            table.addCell(imageQRCode)

        }
        //image.setWidthPercentage(50)

        if(config.barCode!=null){
        imageBarcode.scaleAbsolute(150, 50);

        PdfPCell imageBarcodeCode= new PdfPCell(imageBarcode, false);
        //imageQRCode.addElement();
        imageBarcodeCode.setColspan(3);
        //imageQRCode.setRowspan(3);
        imageBarcodeCode.setHorizontalAlignment(Element.ALIGN_RIGHT)
        imageBarcodeCode.setBorder(Rectangle.NO_BORDER);

        table.addCell(imageBarcodeCode)
        }
        table.addCell(cellEmpty)
        table.addCell(cellEmpty)


        table.addCell(cell)
        table.addCell(cell)


        return table

    }


    def generateTable(cliente, nombreCompleto,  noRecibo, fechaS, granTotal,
                      totalLetra, descripcion, precioUnitario,Configuracion config  ){

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);

        PdfPCell cellEmpty = new PdfPCell(new Phrase("  "));
        cellEmpty.setColspan(6)

        cellEmpty.setBorder(Rectangle.NO_BORDER);
        cellEmpty.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellEmpty.setHorizontalAlignment(Element.ALIGN_CENTER);

        Font smallfont = new Font(Font.FontFamily.HELVETICA, 30);
        Font titleFont = new Font(Font.FontFamily.COURIER, 19);

        PdfPCell cell = new PdfPCell(new Phrase("  "));
        cell.setColspan(6)
        cell.setBackgroundColor(new BaseColor(37,36,64));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell)
        table.addCell(cell)

        table.addCell(cellEmpty)

        def image = Image.getInstance("/tmp/assets/odanLogo.jpeg")
        //image.setWidthPercentage(50)
        //image.scaleAbsolute(100, 50);
        PdfPCell imageCell = new PdfPCell(image, true);
        //imageCell.addElement(,true);
        imageCell.setColspan(3);
        imageCell.setRowspan(6);
        imageCell.setBorder(Rectangle.NO_BORDER);


        PdfPCell nombreEmpresa = new PdfPCell( new Phrase("GRUPO ODAN RED", titleFont))
        nombreEmpresa.setColspan(3)
        nombreEmpresa.setBorder(Rectangle.NO_BORDER);

        PdfPCell servicioNet = new PdfPCell( new Phrase("SERVICIO DE INTERNET", titleFont))
        servicioNet.setColspan(3)
        servicioNet.setBorder(Rectangle.NO_BORDER);

        //PdfPCell permiso = new PdfPCell(new Phrase("IFT/223/UCS/AUT-COM-295/2020"))
        PdfPCell permiso = new PdfPCell(new Phrase(config.permisoIft))
        permiso.setColspan(3)
        permiso.setBorder(Rectangle.NO_BORDER);

        //PdfPCell direccion = new PdfPCell(new Phrase("JOSEFA ORTIZ DE DOMINGUEZ 79"))
        PdfPCell direccion = new PdfPCell(new Phrase(config.direccion))
        direccion.setColspan(3)
        direccion.setBorder(Rectangle.NO_BORDER);


        //PdfPCell codigoPostal = new PdfPCell(new Phrase("CP.:16450"))
        PdfPCell codigoPostal = new PdfPCell(new Phrase("CP.: " + config.codigoPostal))
        codigoPostal.setColspan(3)
        codigoPostal.setBorder(Rectangle.NO_BORDER);

        //PdfPCell telefonoNegocio = new PdfPCell(new Phrase("TEL. 7701092906"))
        PdfPCell telefonoNegocio = new PdfPCell(new Phrase("TEL.: " + config.telefono))
        telefonoNegocio.setColspan(3)
        telefonoNegocio.setBorder(Rectangle.NO_BORDER);


        table.addCell(nombreEmpresa)
        table.addCell(imageCell);

        table.addCell(servicioNet)
        table.addCell(permiso)
        table.addCell(direccion)
        table.addCell(codigoPostal)
        table.addCell(telefonoNegocio)


        table.addCell(cellEmpty)


        PdfPCell a = new PdfPCell(new Phrase("A:"))
        a.setColspan(3)
        a.setBorder(Rectangle.NO_BORDER);


        PdfPCell numero = new PdfPCell(new Phrase("Recibo  $noRecibo"))
        numero.setColspan(3)
        numero.setBorder(Rectangle.NO_BORDER);
        numero.setHorizontalAlignment(Element.ALIGN_RIGHT)

        PdfPCell nombreCliente = new PdfPCell(new Phrase(nombreCompleto))
        nombreCliente.setColspan(3)
        nombreCliente.setBorder(Rectangle.NO_BORDER);

        PdfPCell fecha = new PdfPCell(new Phrase(fechaS))
        fecha.setColspan(3)
        fecha.setRowspan(3)
        fecha.setHorizontalAlignment(Element.ALIGN_RIGHT)
        fecha.setBorder(Rectangle.NO_BORDER);

        PdfPCell direccionCliente = new PdfPCell(new Phrase(cliente.direccion))
        direccionCliente.setColspan(3)
        direccionCliente.setBorder(Rectangle.NO_BORDER);

        table.addCell(a)
        table.addCell(numero)
        table.addCell(nombreCliente)
        table.addCell(fecha)
        table.addCell(direccionCliente)



        if(cliente.telefono!=null){
            PdfPCell telefono = new PdfPCell(new Phrase(cliente.telefono))
            telefono.setColspan(3)
            telefono.setBorder(Rectangle.NO_BORDER);
            table.addCell(telefono)
        }

        table.addCell(cellEmpty)


        PdfPCell totalString = new PdfPCell(new Phrase(" T  O  T  A  L", smallfont))
        totalString.setColspan(3)
        totalString.setFixedHeight(40)
        totalString.setVerticalAlignment(Element.ALIGN_CENTER)
        totalString.setBorder(Rectangle.BOTTOM|Rectangle.TOP);

        PdfPCell totalMonto = new PdfPCell(new Phrase(granTotal,smallfont))
        totalMonto.setColspan(3)
        totalMonto.setHorizontalAlignment(Element.ALIGN_RIGHT)
        totalMonto.setVerticalAlignment(Element.ALIGN_CENTER)
        totalMonto.setBorder(Rectangle.BOTTOM| Rectangle.TOP);


        table.addCell(totalString)
        table.addCell(totalMonto)

        table.addCell(cellEmpty)

        PdfPCell descripcionS = new PdfPCell(new Phrase("D E S C R I P C I Ó N"))
        descripcionS.setColspan(3)
        descripcionS.setBorder(Rectangle.NO_BORDER);

        PdfPCell descripcionServicio = new PdfPCell(new Phrase(descripcion))
        descripcionServicio.setColspan(3)
        descripcionServicio.setBorder(Rectangle.NO_BORDER);

        PdfPCell subtotalS = new PdfPCell(new Phrase("I M P O R T E"))
        subtotalS.setColspan(3)
        subtotalS.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotalS.setBorder(Rectangle.NO_BORDER);


        PdfPCell subtotal = new PdfPCell(new Phrase(granTotal))
        subtotal.setColspan(3)
        subtotal.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotal.setBorder(Rectangle.NO_BORDER);


        table.addCell(descripcionS)
        table.addCell(subtotalS)
        table.addCell(cellEmpty)
        table.addCell(descripcionServicio)
        table.addCell(subtotal)

        table.addCell(cellEmpty)

        PdfPCell totalLetraCell = new PdfPCell(new Phrase(totalLetra))
        totalLetraCell.setColspan(6)
        totalLetraCell.setBorder(Rectangle.NO_BORDER);

        table.addCell(totalLetraCell)
        //table.addCell(cellEmpty)
        table.addCell(cellEmpty)

        PdfPCell diaPago = new PdfPCell(new Phrase("Día de pago: " +cliente.diaCorte +" de cada mes."))
        diaPago.setColspan(3)
        //diaPago.setHorizontalAlignment(Element.ALIGN_RIGHT)
        diaPago.setBorder(Rectangle.NO_BORDER);

        table.addCell(diaPago)
        table.addCell(cellEmpty)


        PdfPCell condiciones = new PdfPCell(new Phrase("CONDICIONES Y FORMA DE PAGO"))
        condiciones.setColspan(6)
        condiciones.setBorder(Rectangle.NO_BORDER);

        table.addCell(condiciones)

        PdfPCell condicionesString = new PdfPCell(new Phrase("TIEMPO LIMITE DE PAGO $config.diasGraciaCorte DIAS HABILES DESPUES DE LA FECHA INDICADA"))
        condicionesString.setColspan(6)
        condicionesString.setBorder(Rectangle.NO_BORDER);

        table.addCell(condicionesString)

        table.addCell(cellEmpty)

        def imageQR = Image.getInstance("/tmp/assets/odanQrCode.jpeg")
        //image.setWidthPercentage(50)
        imageQR.scaleAbsolute(100, 100);

        PdfPCell imageQRCode = new PdfPCell(imageQR, false);
        //imageQRCode.addElement();
        imageQRCode.setColspan(6);
        //imageQRCode.setRowspan(3);
        imageQRCode.setHorizontalAlignment(Element.ALIGN_RIGHT)
        imageQRCode.setBorder(Rectangle.NO_BORDER);

        table.addCell(imageQRCode)
        table.addCell(cellEmpty)

        table.addCell(cell)
        table.addCell(cell)






        return table

    }


    def generateTableFusion(cliente, nombreCompleto, noRecibo, fechaS, granTotal, totalLetra, descripcion, precioUnitario, config ){

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);

        PdfPCell cellEmpty = new PdfPCell(new Phrase("  "));
        cellEmpty.setColspan(6)

        cellEmpty.setBorder(Rectangle.NO_BORDER);
        cellEmpty.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellEmpty.setHorizontalAlignment(Element.ALIGN_CENTER);

        Font smallfont = new Font(Font.FontFamily.HELVETICA, 30);
        Font titleFont = new Font(Font.FontFamily.COURIER, 19);

        PdfPCell cell = new PdfPCell(new Phrase("  "));
        cell.setColspan(6)
        cell.setBackgroundColor(new BaseColor(0,38,94));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell)
        table.addCell(cell)

        table.addCell(cellEmpty)

        def image = Image.getInstance("/tmp/assets/fusionLogo.png")
        //image.setWidthPercentage(50)
        //image.scaleAbsolute(100, 50);
        PdfPCell imageCell = new PdfPCell(image, true);
        //imageCell.addElement(,true);
        imageCell.setColspan(3);
        imageCell.setRowspan(7);
        imageCell.setBorder(Rectangle.NO_BORDER);


        PdfPCell nombreEmpresa = new PdfPCell( new Phrase("FUSION NETWORK", titleFont))
        nombreEmpresa.setColspan(3)
        nombreEmpresa.setBorder(Rectangle.NO_BORDER);

        PdfPCell servicioNet = new PdfPCell( new Phrase("SERVICIO DE INTERNET", titleFont))
        servicioNet.setColspan(3)
        servicioNet.setBorder(Rectangle.NO_BORDER);

        //PdfPCell permiso = new PdfPCell(new Phrase("IFT/223/UCS/AUT-COM-295/2020"))
        //permiso.setColspan(3)
        //permiso.setBorder(Rectangle.NO_BORDER);

        //PdfPCell direccion = new PdfPCell(new Phrase("PLAZA DE LA CONSTITUCION"))
        //direccion.setColspan(3)
        //direccion.setBorder(Rectangle.NO_BORDER);

        PdfPCell localidad = new PdfPCell(new Phrase("BOMINTZHÁ"))
        localidad.setColspan(3)
        localidad.setBorder(Rectangle.NO_BORDER);

        PdfPCell estado = new PdfPCell(new Phrase("TULA DE ALLENDE HGO."))
        estado.setColspan(3)
        estado.setBorder(Rectangle.NO_BORDER);

        PdfPCell codigoPostal = new PdfPCell(new Phrase("CP.:42832"))
        codigoPostal.setColspan(3)
        codigoPostal.setBorder(Rectangle.NO_BORDER);

        //PdfPCell telefonoNegocio = new PdfPCell(new Phrase("TEL. 7701092906"))
        //telefonoNegocio.setColspan(3)
        //telefonoNegocio.setBorder(Rectangle.NO_BORDER);


        table.addCell(nombreEmpresa)
        table.addCell(imageCell);

        table.addCell(servicioNet)
        //table.addCell(permiso)
        //table.addCell(direccion)
        table.addCell(localidad)
        table.addCell(estado)
        table.addCell(codigoPostal)
        //table.addCell(telefonoNegocio)


        table.addCell(cellEmpty)


        PdfPCell a = new PdfPCell(new Phrase("A:"))
        a.setColspan(3)
        a.setBorder(Rectangle.NO_BORDER);


        PdfPCell numero = new PdfPCell(new Phrase("Recibo  $noRecibo"))
        numero.setColspan(3)
        numero.setBorder(Rectangle.NO_BORDER);
        numero.setHorizontalAlignment(Element.ALIGN_RIGHT)

        PdfPCell nombreCliente = new PdfPCell(new Phrase(nombreCompleto))
        nombreCliente.setColspan(3)
        nombreCliente.setBorder(Rectangle.NO_BORDER);

        PdfPCell fecha = new PdfPCell(new Phrase(fechaS))
        fecha.setColspan(3)
        fecha.setRowspan(3)
        fecha.setHorizontalAlignment(Element.ALIGN_RIGHT)
        fecha.setBorder(Rectangle.NO_BORDER);

        PdfPCell direccionCliente = new PdfPCell(new Phrase(cliente.direccion))
        direccionCliente.setColspan(3)
        direccionCliente.setBorder(Rectangle.NO_BORDER);

        table.addCell(a)
        table.addCell(numero)
        table.addCell(nombreCliente)
        table.addCell(fecha)
        table.addCell(direccionCliente)



        if(cliente.telefono!=null){
            PdfPCell telefono = new PdfPCell(new Phrase(cliente.telefono))
            telefono.setColspan(3)
            telefono.setBorder(Rectangle.NO_BORDER);
            table.addCell(telefono)
        }

        table.addCell(cellEmpty)
        table.addCell(cellEmpty)



        PdfPCell totalString = new PdfPCell(new Phrase(" T  O  T  A  L", smallfont))
        totalString.setColspan(3)
        totalString.setFixedHeight(40)
        totalString.setVerticalAlignment(Element.ALIGN_CENTER)
        totalString.setBorder(Rectangle.BOTTOM|Rectangle.TOP);

        PdfPCell totalMonto = new PdfPCell(new Phrase(granTotal,smallfont))
        totalMonto.setColspan(3)
        totalMonto.setHorizontalAlignment(Element.ALIGN_RIGHT)
        totalMonto.setVerticalAlignment(Element.ALIGN_CENTER)
        totalMonto.setBorder(Rectangle.BOTTOM| Rectangle.TOP);


        table.addCell(totalString)
        table.addCell(totalMonto)

        table.addCell(cellEmpty)
        table.addCell(cellEmpty)

        PdfPCell descripcionS = new PdfPCell(new Phrase("D E S C R I P C I Ó N"))
        descripcionS.setColspan(3)
        descripcionS.setBorder(Rectangle.NO_BORDER);

        PdfPCell descripcionServicio = new PdfPCell(new Phrase(descripcion))
        descripcionServicio.setColspan(3)
        descripcionServicio.setBorder(Rectangle.NO_BORDER);

        PdfPCell subtotalS = new PdfPCell(new Phrase("I M P O R T E"))
        subtotalS.setColspan(3)
        subtotalS.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotalS.setBorder(Rectangle.NO_BORDER);


        PdfPCell subtotal = new PdfPCell(new Phrase(granTotal))
        subtotal.setColspan(3)
        subtotal.setHorizontalAlignment(Element.ALIGN_RIGHT)
        subtotal.setBorder(Rectangle.NO_BORDER);


        table.addCell(descripcionS)
        table.addCell(subtotalS)
        table.addCell(cellEmpty)
        table.addCell(descripcionServicio)
        table.addCell(subtotal)

        table.addCell(cellEmpty)
        //table.addCell(cellEmpty)

        PdfPCell totalLetraCell = new PdfPCell(new Phrase(totalLetra))
        totalLetraCell.setColspan(6)
        totalLetraCell.setBorder(Rectangle.NO_BORDER);

        table.addCell(totalLetraCell)
        //table.addCell(cellEmpty)
        table.addCell(cellEmpty)

        PdfPCell diaPago = new PdfPCell(new Phrase("Día de pago: " +cliente.diaCorte +" de cada mes."))
        diaPago.setColspan(3)
        //diaPago.setHorizontalAlignment(Element.ALIGN_RIGHT)
        diaPago.setBorder(Rectangle.NO_BORDER);

        table.addCell(diaPago)
        table.addCell(cellEmpty)
        table.addCell(cellEmpty)

        PdfPCell condiciones = new PdfPCell(new Phrase("CONDICIONES Y FORMA DE PAGO"))
        condiciones.setColspan(6)
        condiciones.setBorder(Rectangle.NO_BORDER);

        table.addCell(condiciones)

        PdfPCell condicionesString = new PdfPCell(new Phrase("TIEMPO LIMITE DE PAGO 5 DIAS HABILES DESPUES DE LA FECHA INDICADA"))
        condicionesString.setColspan(6)
        condicionesString.setBorder(Rectangle.NO_BORDER);

        table.addCell(condicionesString)

        table.addCell(cellEmpty)

        def imageQR = Image.getInstance("/tmp/assets/fusionQrCode.png")
        //image.setWidthPercentage(50)
        imageQR.scaleAbsolute(100, 100);

        PdfPCell imageQRCode = new PdfPCell(imageQR, false);
        //imageQRCode.addElement();
        imageQRCode.setColspan(6);
        //imageQRCode.setRowspan(3);
        imageQRCode.setHorizontalAlignment(Element.ALIGN_RIGHT)
        imageQRCode.setBorder(Rectangle.NO_BORDER);

        table.addCell(imageQRCode)
        table.addCell(cellEmpty)

        table.addCell(cell)
        table.addCell(cell)




        return table

    }

    def pagosMesActual(){
        return '$1040.56'
    }

    def generarPagos(){


        println params
        println 'Entra a generar los pagos'

        //Validar que sea un año valido

        def year = params.year.toInteger()

        //obtener los clientes

        def clientesList = Cliente.list()

        clientesList.each {
            generateHistorial(it,year)
        }


        flash.message = 'Se han generado los pagos para el año ' + params.year
        redirect( controller:'configuracion', action: 'edit', id: 1)
    }


    def generateHistorial(Cliente cliente, int year) {

        LocalDate dt = LocalDate.of(year, 1, 1);
        //String month = dt.month;  // where January is 1 and December is 12
        //int year = dt.year;

        LocalDate lastDayOfYear = LocalDate.of(year, Month.DECEMBER,31)

        Period period = dt.until(lastDayOfYear);


        def months = period.getMonths() + 1
        months.times {
            def historial = new Historial()
            historial.setCliente(cliente)
            historial.setMes(dt.plusMonths(it).getMonthValue()) //todo guardar mes actual
            historial.setAnio(year) //todo guardar año acutal
            historial.setStatusPago(false) //para la primera vez siempre sera pendiente
            historialService.save(historial)
        }
    }


    def suspenderServicio(Long id){
        def historial = Historial.get(id)
        def cliente = historial.cliente
        println "cliente" + cliente

        if (cliente.statusServicio.equals("EN_LINEA")){
            mikrotikHelperService.suspenderServicio(cliente)
        }
        cliente.statusServicio= "SUSPENDIDO"
        clienteService.save(cliente)
        flash.message = 'Se ha suspendido el servicio con exito'
        redirect(action: 'show', id:historial.cliente.id)

    }

    def reanudarServicio(Long id){
        def historial = Historial.get(id)
        def cliente = historial.cliente
        println "cliente" + cliente

        def conexion = cliente.mikrotik

        if (cliente.statusServicio.equals("SUSPENDIDO")){
            mikrotikHelperService.reactivarServicio(cliente)
        }
        cliente.statusServicio= "EN_LINEA"
        clienteService.save(cliente)
        flash.message = 'Se ha reactivado el servicio con exito'
        redirect(action: 'show', id:historial.cliente.id)
    }



}
