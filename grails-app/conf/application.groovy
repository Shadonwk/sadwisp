

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.logout.postOnly = false

grails.plugin.springsecurity.authority.nameField = 'nombre'
grails.plugin.springsecurity.userLookup.userDomainClassName = 'mx.com.wisnet.sadwisp.seguridad.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'mx.com.wisnet.sadwisp.seguridad.UsuarioRol'
grails.plugin.springsecurity.authority.className = 'mx.com.wisnet.sadwisp.seguridad.Rol'


grails.plugin.springsecurity.controllerAnnotations.staticRules = [

		[pattern: '/error',          access: ['permitAll']],
		[pattern: '/index',          access: ['permitAll']],
		[pattern: '/index.gsp',      access: ['permitAll']],
		[pattern: '/shutdown',       access: ['permitAll']],
		[pattern: '/assets/**',      access: ['permitAll']],
		[pattern: '/**/js/**',       access: ['permitAll']],
		[pattern: '/**/css/**',      access: ['permitAll']],
		[pattern: '/**/images/**',   access: ['permitAll']],
		[pattern: '/**/favicon.ico', access: ['permitAll']],
		[pattern: '/usuario/**',     access: ['ROLE_ADMINISTRADOR']],
		[pattern: '/**',     access: ['ROLE_ADMINISTRADOR']],

	[pattern: '/plan/**',     access: ['ROLE_ADMINISTRADOR']],
	[pattern: '/mikrotik/**',     access: ['ROLE_ADMINISTRADOR']],
	[pattern: '/cliente/**',     access: ['ROLE_ADMINISTRADOR', 'ROLE_COBRADOR', 'ROLE_TECNICO']],
	[pattern: '/reportes/**',     access: ['ROLE_ADMINISTRADOR', 'ROLE_COBRADOR']],
	[pattern: '/historial/**',     access: ['ROLE_ADMINISTRADOR','ROLE_COBRADOR']],
	[pattern: '/notificacion/**',     access: ['ROLE_ADMINISTRADOR']],
	[pattern: '/configuracion/**',     access: ['ROLE_ADMINISTRADOR']],
	[pattern: '/ticket/**',     access: ['ROLE_ADMINISTRADOR','ROLE_TECNICO']],
	[pattern: '/escritorio/**',     access: ['ROLE_ADMINISTRADOR']]

]

//grails.plugin.springsecurity.controllerAnnotations.staticRules = [
//	[pattern: '/',               access: ['permitAll']],
//	[pattern: '/error',          access: ['permitAll']],
//	[pattern: '/index',          access: ['ROLE_ADMINISTRADOR','ROLE_USUARIO','ROLE_COBRADOR']],
//	[pattern: '/usuario/**',     access: ['ROLE_ADMINISTRADOR']],
//	[pattern: '/plan/**',     access: ['ROLE_ADMINISTRADOR']],
//	[pattern: '/mikrotik/**',     access: ['ROLE_ADMINISTRADOR']],
//	//[pattern: '/cliente/**',     access: ['ROLE_ADMINISTRADOR', 'ROLE_COBRADOR']],
//	[pattern: '/cliente/**',     access: ['permitAll']],
//	[pattern: '/reportes/**',     access: ['ROLE_ADMINISTRADOR', 'ROLE_COBRADOR']],
//	[pattern: '/historial/**',     access: ['ROLE_ADMINISTRADOR','ROLE_COBRADOR']],
//	[pattern: '/notificacion/**',     access: ['ROLE_ADMINISTRADOR']],
//	[pattern: '/configuracion/**',     access: ['ROLE_ADMINISTRADOR']],
//	[pattern: '/escritorio/**',     access: ['ROLE_ADMINISTRADOR']],
//	//[pattern: '/api/**', filters:'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter'],

//
//
//	[pattern: '/shutdown',       access: ['permitAll']],
//	[pattern: '/assets/**',      access: ['permitAll']],
//	[pattern: '/**/reports/**',      access: ['permitAll']],
//	[pattern: '/jasper/**' ,access: ['permitAll']],
//	[pattern: '/**/js/**',       access: ['permitAll']],
//	[pattern: '/**/css/**',      access: ['permitAll']],
//	[pattern: '/**/images/**',   access: ['permitAll']],
//	[pattern: '/images/**',   access: ['permitAll']],
//	[pattern: '/**/fonts/**',   access: ['permitAll']],
//	[pattern: '/dbconsole/**',   access: ['permitAll']],
//	[pattern: '/**/favicon.ico', access: ['permitAll']]
//
//
//]

grails.plugin.springsecurity.filterChain.chainMap = [
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],
		[pattern: '/api2/**', filters:'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter'],
		[pattern: '/api/**', filters:'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter'],
		[pattern: '/**', filters:'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter']
]


grails.plugin.springsecurity.rest.logout.endpointUrl = '/api/logout'
grails.plugin.springsecurity.rest.token.validation.useBearerToken = false
grails.plugin.springsecurity.rest.token.validation.headerName = 'X-Auth-Token'
grails.plugin.springsecurity.rest.token.storage.jwt.expiration = 36000


grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.logout.afterLogoutUrl = '/login/auth'

