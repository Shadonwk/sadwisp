SET IDENTITY_INSERT rol ON
insert into rol (id, version, nombre) values (1, 0, 'ROLE_ADMINISTRADOR');

-- ROLES PARA REGISTROS
insert into rol (id, version, nombre) values (2, 0, 'ROLE_USUARIO');
insert into rol (id, version, nombre) values (3, 0, 'ROLE_COBRADOR');
insert into rol (id, version, nombre) values (4, 0, 'ROLE_TECNICO');
SET IDENTITY_INSERT rol OFF

-- usuario
SET IDENTITY_INSERT usuario ON
insert into usuario (id, version, username, "password", enabled, account_expired, account_locked, password_expired, nombre, apellido_paterno, apellido_materno, email, fecha_alta) values (1, 0, 'administrador', '$2a$10$ObRvaOIJ3rf1xEe0yM7bTeV66gGceJaQo2OUX/6hhLbBFAu74tdJe', 1, 0 , 0, 0, 'Administrador', 'del', 'Sistema', 'administrador@example.com', current_timestamp);
--insert into usuario (id, version, username, "password", enabled, account_expired, account_locked, password_expired, nombre, apellido_paterno, apellido_materno, email, fecha_alta) values (1, 0, 'administrador', 'c70cee7fb85a185c90f4e1092a2fb5522bca6837463aa7bb733cf907ea5d2f62', true, false , false, false, 'Administrador', 'del', 'Sistema', 'administrador@example.com', current_timestamp);
SET IDENTITY_INSERT usuario OFF


-- usuario_rol
insert into usuario_rol (usuario_id, rol_id) values (1, 1);


--SET IDENTITY_INSERT mikrotik ON
--insert into mikrotik(id, version,  ip_Mikrotik, usuario_Mikrotik, password_Mikrotik, puerto, nombre) values (1, 0, '0.0.0.0', 'admin', 'admin', 8728, 'MikrotikPrueba');
--SET IDENTITY_INSERT mikrotik OFF


SET IDENTITY_INSERT configuracion ON
insert into configuracion(id, version, consecutivo_notas, direccion_fecha,dias_gracia_corte,dias_recordatorio, mensaje_corte, mensaje_recordatorio, id_dispositivo, key_dispositivo, editar_nota, post_pago, nombre_logo, secret_manual, secret_update)values(1,0,1,'Bomintzhá Hgo.',2,2, 'Su servicio se ha suspendido por falta de pago','Su fecha de pago es el día','NA','NA', 0, 0, 'FUSION.png', 0, 0 );
SET IDENTITY_INSERT configuracion OFF


--SET IDENTITY_INSERT tarifa ON
--insert into tarifa(id, version, nombre, costo, mikrotik_id, tipo, upload, download, upload_medida, download_medida, burst_Limit, burst_Threshold, burst_Time, parent)values (1,0,'Básico', 300.0, 1, 'Simple Queue', '5', '10', 'MB', 'MB', '20', '10', '30', 'Total' );
--insert into tarifa(id, version, nombre, costo, mikrotik_id, tipo, upload, download, upload_medida, download_medida, burst_Limit, burst_Threshold, burst_Time, parent)values (2,0,'Medio', 300.0, 1, 'Simple Queue', '5', '10', 'MB', 'MB', '20', '10', '30', 'Total' );
--insert into tarifa(id, version, nombre, costo, mikrotik_id, tipo, upload, download, upload_medida, download_medida, burst_Limit, burst_Threshold, burst_Time, parent)values (3,0,'Plus', 300.0, 1, 'Simple Queue', '5', '10', 'MB', 'MB', '20', '10', '30', 'Total' );
--SET IDENTITY_INSERT plan OFF





