function getGraficaPagos(pagadoEnMes, porCobrarEnMes) {
    var labels = ['Pagado', 'Por Cobrar']
    var colors = ['rgb(6,152,245)', 'rgb(239,193,12)'];

    var graph1 = document.querySelector("#grafica");

    var data = {
        labels: labels,
        datasets: [{
            data: [pagadoEnMes, porCobrarEnMes, ],
            backgroundColor: colors
        }]
    };
    var config = {
        type: 'pie',
        data: data,
    };

    new Chart(graph1, config);
}

function getGraficaNuevosClientes(semana1, semana2, semana3, semana4) {

    var labels = ['Semana 1', 'Semana 2', 'Semana 3', 'Semana 4']
    var colors = ['rgb(217,65,146)'];

    var graph1 = document.querySelector("#grafica2");

    var data = {
        labels: labels,
        datasets: [{
            label:"Nuevos clientes por semana",
            data: [semana1, semana2, semana3, semana4, ],
            backgroundColor: colors
        }]
    };
    var config = {
        type: 'bar',
        data: data,
    };

    new Chart(graph1, config);
}

