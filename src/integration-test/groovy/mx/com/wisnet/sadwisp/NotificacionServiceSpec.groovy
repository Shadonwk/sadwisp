package mx.com.wisnet.sadwisp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class NotificacionServiceSpec extends Specification {

    NotificacionService notificacionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Notificacion(...).save(flush: true, failOnError: true)
        //new Notificacion(...).save(flush: true, failOnError: true)
        //Notificacion notificacion = new Notificacion(...).save(flush: true, failOnError: true)
        //new Notificacion(...).save(flush: true, failOnError: true)
        //new Notificacion(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //notificacion.id
    }

    void "test get"() {
        setupData()

        expect:
        notificacionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Notificacion> notificacionList = notificacionService.list(max: 2, offset: 2)

        then:
        notificacionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        notificacionService.count() == 5
    }

    void "test delete"() {
        Long notificacionId = setupData()

        expect:
        notificacionService.count() == 5

        when:
        notificacionService.delete(notificacionId)
        sessionFactory.currentSession.flush()

        then:
        notificacionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Notificacion notificacion = new Notificacion()
        notificacionService.save(notificacion)

        then:
        notificacion.id != null
    }
}
