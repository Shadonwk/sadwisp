package mx.com.wisnet.sadwisp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ConfiguracionServiceSpec extends Specification {

    ConfiguracionService configuracionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Configuracion(...).save(flush: true, failOnError: true)
        //new Configuracion(...).save(flush: true, failOnError: true)
        //Configuracion configuracion = new Configuracion(...).save(flush: true, failOnError: true)
        //new Configuracion(...).save(flush: true, failOnError: true)
        //new Configuracion(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //configuracion.id
    }

    void "test get"() {
        setupData()

        expect:
        configuracionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Configuracion> configuracionList = configuracionService.list(max: 2, offset: 2)

        then:
        configuracionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        configuracionService.count() == 5
    }

    void "test delete"() {
        Long configuracionId = setupData()

        expect:
        configuracionService.count() == 5

        when:
        configuracionService.delete(configuracionId)
        sessionFactory.currentSession.flush()

        then:
        configuracionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Configuracion configuracion = new Configuracion()
        configuracionService.save(configuracion)

        then:
        configuracion.id != null
    }
}
