package mx.com.wisnet.sadwisp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class HistorialServiceSpec extends Specification {

    HistorialService historialService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Historial(...).save(flush: true, failOnError: true)
        //new Historial(...).save(flush: true, failOnError: true)
        //Historial historial = new Historial(...).save(flush: true, failOnError: true)
        //new Historial(...).save(flush: true, failOnError: true)
        //new Historial(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //historial.id
    }

    void "test get"() {
        setupData()

        expect:
        historialService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Historial> historialList = historialService.list(max: 2, offset: 2)

        then:
        historialList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        historialService.count() == 5
    }

    void "test delete"() {
        Long historialId = setupData()

        expect:
        historialService.count() == 5

        when:
        historialService.delete(historialId)
        sessionFactory.currentSession.flush()

        then:
        historialService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Historial historial = new Historial()
        historialService.save(historial)

        then:
        historial.id != null
    }
}
